﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SocialNetwork.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Photos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PhotoName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<int>(nullable: false),
                    Bio = table.Column<string>(nullable: true),
                    JobTitle = table.Column<string>(nullable: true),
                    IsPrivate = table.Column<bool>(nullable: false),
                    PhotoId = table.Column<int>(nullable: true),
                    PhotoName = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Photos_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "Photos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FriendRequests",
                columns: table => new
                {
                    SenderId = table.Column<Guid>(nullable: false),
                    ReceiverId = table.Column<Guid>(nullable: false),
                    FriendRequestStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FriendRequests", x => new { x.ReceiverId, x.SenderId });
                    table.ForeignKey(
                        name: "FK_FriendRequests_AspNetUsers_ReceiverId",
                        column: x => x.ReceiverId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FriendRequests_AspNetUsers_SenderId",
                        column: x => x.SenderId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    Content = table.Column<string>(nullable: false),
                    AccessStatus = table.Column<int>(nullable: false),
                    PhotoId = table.Column<int>(nullable: true),
                    VideoUrl = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Photos_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "Photos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Posts_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "UserFriends",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    FriendId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserFriends", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserFriends_AspNetUsers_FriendId",
                        column: x => x.FriendId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserFriends_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    PostId = table.Column<int>(nullable: false),
                    Content = table.Column<string>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comments_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Likes",
                columns: table => new
                {
                    PostId = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    LikeType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Likes", x => new { x.PostId, x.UserId });
                    table.ForeignKey(
                        name: "FK_Likes_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Likes_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("177513b2-a3fb-4c13-a481-39bff26141c3"), "3203fb79-2bd0-4483-9f13-7b19dca39a29", "User", "USER" },
                    { new Guid("50ee8e13-16ce-4168-8c30-13e244c4497e"), "b0bfaf54-9259-4d81-a874-d0facb00354f", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "Bio", "ConcurrencyStamp", "Country", "CreatedOn", "DateOfBirth", "DeletedOn", "Email", "EmailConfirmed", "FirstName", "Gender", "IsDeleted", "IsPrivate", "JobTitle", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "PhotoId", "PhotoName", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("2d0760d0-82c2-47c4-b5a1-5924d69ca5db"), 0, null, "6de92271-f304-4360-b459-0f8c7d8aa3ee", "France", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 5, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "nathanhill@mail.com", true, "Nathan", 0, false, false, null, "Hill", false, null, "NATHANHILL@MAIL.COM", "NATHANHILL@MAIL.COM", "AQAAAAEAACcQAAAAEFULzBlL1/C6U/TKwRnqg/FKZvipgzwfYGyamkoWv0q90fK0EVewxpg2HgNkK54fpA==", null, false, null, "nathanHill20143094.jpg", "a13a52f3-523f-4dc3-ad93-07d88bfc5789", false, "nathanhill@mail.com" },
                    { new Guid("4922b615-00ad-4ded-b733-fac0e52b6463"), 0, null, "be959f18-8c65-44e0-b52f-3454a0b99c7d", "Bulgaria", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1998, 9, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "antonia@mail.com", true, "Antonia", 1, false, false, null, "Dimitrova", false, null, "ANTONIA@MAIL.COM", "ANTONIA@MAIL.COM", "AQAAAAEAACcQAAAAENDoWnBqiN8bgJ4JBcOnMi8bxxX3OszmMNf+qlCwlKOC8pIEm9ye/tI/1OWNOxQ5Mw==", null, false, null, "antonia20163919.jpg", "0561c844-442a-4c24-a86b-4ae67aa51d3e", false, "antonia@mail.com" }
                });

            migrationBuilder.InsertData(
                table: "Photos",
                columns: new[] { "Id", "PhotoName" },
                values: new object[,]
                {
                    { 13, "seal20300244.jpg" },
                    { 12, "savanah20183396.jpg" },
                    { 11, "nightsky20082107.jpg" },
                    { 10, "dolpin20570548.jpeg" },
                    { 9, "antonia20163919.jpg" },
                    { 7, "me20185292.jpg" },
                    { 6, "9420511100.jpg" },
                    { 5, "photo_large20554592" },
                    { 4, "3620514289.jpg" },
                    { 3, "2020502529.jpg" },
                    { 2, "night20374837.jpg" },
                    { 8, "nathanHill20143094.jpg" },
                    { 1, "profilePicture" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("4922b615-00ad-4ded-b733-fac0e52b6463"), new Guid("177513b2-a3fb-4c13-a481-39bff26141c3") },
                    { new Guid("2d0760d0-82c2-47c4-b5a1-5924d69ca5db"), new Guid("177513b2-a3fb-4c13-a481-39bff26141c3") }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "Bio", "ConcurrencyStamp", "Country", "CreatedOn", "DateOfBirth", "DeletedOn", "Email", "EmailConfirmed", "FirstName", "Gender", "IsDeleted", "IsPrivate", "JobTitle", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "PhotoId", "PhotoName", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("b6de00f9-b6ac-497b-9ba2-6df1b38a8f24"), 0, null, "06615d73-fec7-4f84-bfc7-b0d7fca2f129", "Bulgaria", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1982, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "goshi@gmail.com", true, "Georgi", 0, false, false, null, "Ivanov", false, null, "GOSHI@GMAIL.COM", "GOSHI@GMAIL.COM", "AQAAAAEAACcQAAAAEMjud9TjPpphChftyuI2m+Y6JCHHT1PTMBG2vSCSKENEfMJgk9vUJM7N2+Oxklnaug==", null, false, 3, "2020502529.jpg", "b656f8ad-c284-4d51-9dd3-cf30267f06b0", false, "goshi@gmail.com" },
                    { new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), 0, null, "2725059c-3fb1-43b5-bb29-ecbedee16fa9", "Bulgaria", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "borimirov@gmail.com", true, "Bob", 0, false, false, null, "Borimirov", false, null, "BORIMIROV@GMAIL.COM", "BORIMIROV@GMAIL.COM", "AQAAAAEAACcQAAAAECcm0E76btRDneIrdTbrcSEJduMj4rmofVysEJmnlY+W0jKoK7JY2LI2UXKNlrfNSA==", null, false, 4, "3620514289.jpg", "2d1eee6c-605c-49e0-af43-772b2d3be067", false, "borimirov@gmail.com" },
                    { new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55"), 0, null, "96c5b86b-9d75-452c-a4c8-5149300deebf", "Bulgaria", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 8, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "alice@gmail.com", true, "Alice", 1, false, false, null, "Dimitrova", false, null, "ALICE@GMAIL.COM", "ALICE@GMAIL.COM", "AQAAAAEAACcQAAAAEDikfr++ZX1RU5zAUZ6b4doFmFV17urnKFrNKn2T0NLndGxPEpGqMnWexmGKwTMrsQ==", null, false, 5, "photo_large20554592.jpg", "b779329b-9131-49e9-949c-737cf74d1e37", false, "alice@gmail.com" },
                    { new Guid("0fdd5513-78ca-46de-853e-50cea86e5db1"), 0, null, "24eb8ecb-92ad-4ea6-acc9-ee3d11e6697e", "Bulgaria", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1988, 6, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "ggeorgiev@gmail.com", true, "Georgi", 0, false, false, null, "Georgiev", false, null, "GGEORGIEV@GMAIL.COM", "GGEORGIEV@GMAIL.COM", "AQAAAAEAACcQAAAAEPCqKNH4/LHxCgm5FxNdGbZl2JvqR5TeMh81ON4UJzKpwTFie46wJzvorGq39A1d2Q==", null, false, 6, "9420511100.jpg", "c060b2a1-bc87-46f1-888f-b0b2aba5c898", false, "ggeorgiev@gmail.com" },
                    { new Guid("202ac5cd-afa2-40f7-bbb6-416fddcbcc2b"), 0, null, "12ee9607-978e-4710-9a7a-2fdab35f0337", "Bulgaria", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1994, 7, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "mariyan@gmail.com", true, "Mariyan", 0, false, true, null, "Bogomilov", false, null, "MARIYAN@GMAIL.COM", "MARIYAN@GMAIL.COM", "AQAAAAEAACcQAAAAENQ/q4fNyjVYv2vc4AjawJ9PlgMMP3iz4tX2ZPQ/+ASfldVxqHBoV6sLuCXnkbaAXQ==", null, false, 7, "me20185292.jpg", "4f5f8fe7-566f-42a3-9850-1768d380e925", false, "mariyan@gmail.com" }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AccessStatus", "Content", "CreatedOn", "DeletedOn", "IsDeleted", "PhotoId", "UserId", "VideoUrl" },
                values: new object[,]
                {
                    { 10, 1, "Dolphin tricks", new DateTime(2020, 11, 12, 7, 22, 16, 0, DateTimeKind.Unspecified), null, false, 11, new Guid("4922b615-00ad-4ded-b733-fac0e52b6463"), null },
                    { 11, 0, "Sea Life Oberhausen", new DateTime(2020, 11, 17, 7, 50, 16, 0, DateTimeKind.Unspecified), null, false, null, new Guid("4922b615-00ad-4ded-b733-fac0e52b6463"), "https://www.youtube.com/embed/_EaMFf97Ab8" },
                    { 3, 0, "From Earth To the Milky Way Galaxy", new DateTime(2020, 11, 17, 13, 5, 12, 0, DateTimeKind.Unspecified), null, false, null, new Guid("2d0760d0-82c2-47c4-b5a1-5924d69ca5db"), "https://www.youtube.com/embed/0La58bUNNzQ" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("b6de00f9-b6ac-497b-9ba2-6df1b38a8f24"), new Guid("177513b2-a3fb-4c13-a481-39bff26141c3") },
                    { new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), new Guid("177513b2-a3fb-4c13-a481-39bff26141c3") },
                    { new Guid("202ac5cd-afa2-40f7-bbb6-416fddcbcc2b"), new Guid("50ee8e13-16ce-4168-8c30-13e244c4497e") },
                    { new Guid("0fdd5513-78ca-46de-853e-50cea86e5db1"), new Guid("50ee8e13-16ce-4168-8c30-13e244c4497e") },
                    { new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55"), new Guid("177513b2-a3fb-4c13-a481-39bff26141c3") }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "CreatedOn", "DeletedOn", "IsDeleted", "PostId", "UserId" },
                values: new object[] { 7, "Extraordinary sea life", new DateTime(2020, 10, 26, 11, 14, 16, 0, DateTimeKind.Unspecified), null, false, 11, new Guid("4922b615-00ad-4ded-b733-fac0e52b6463") });

            migrationBuilder.InsertData(
                table: "FriendRequests",
                columns: new[] { "ReceiverId", "SenderId", "FriendRequestStatus" },
                values: new object[,]
                {
                    { new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55"), 1 },
                    { new Guid("0fdd5513-78ca-46de-853e-50cea86e5db1"), new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), 1 },
                    { new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55"), new Guid("0fdd5513-78ca-46de-853e-50cea86e5db1"), 1 },
                    { new Guid("b6de00f9-b6ac-497b-9ba2-6df1b38a8f24"), new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), 1 }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AccessStatus", "Content", "CreatedOn", "DeletedOn", "IsDeleted", "PhotoId", "UserId", "VideoUrl" },
                values: new object[,]
                {
                    { 9, 0, "Time for a nap", new DateTime(2020, 11, 17, 13, 5, 12, 0, DateTimeKind.Unspecified), null, false, 13, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), null },
                    { 4, 0, "Night Sky", new DateTime(2020, 9, 17, 13, 5, 12, 0, DateTimeKind.Unspecified), null, false, null, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), "https://www.youtube.com/embed/jfVpwHrNzMA" },
                    { 6, 1, "Four Seasons", new DateTime(2020, 11, 27, 13, 5, 12, 0, DateTimeKind.Unspecified), null, false, null, new Guid("202ac5cd-afa2-40f7-bbb6-416fddcbcc2b"), "https://www.youtube.com/embed/GRxofEmo3HA" },
                    { 5, 0, "Savannah", new DateTime(2020, 10, 17, 13, 5, 12, 0, DateTimeKind.Unspecified), null, false, 12, new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55"), null },
                    { 2, 1, "Thunder struck", new DateTime(2020, 11, 17, 17, 50, 16, 0, DateTimeKind.Unspecified), null, false, 11, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), null },
                    { 8, 0, "The Breathtaking Beauty of Nature", new DateTime(2020, 11, 22, 11, 15, 12, 0, DateTimeKind.Unspecified), null, false, null, new Guid("0fdd5513-78ca-46de-853e-50cea86e5db1"), "https://www.youtube.com/embed/IUN664s7N-c" },
                    { 7, 1, "Top 20 Best Football Players of All Time", new DateTime(2020, 11, 27, 15, 5, 12, 0, DateTimeKind.Unspecified), null, false, null, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), "https://www.youtube.com/embed/rA1102ZzprY" },
                    { 1, 0, "Legendary goals", new DateTime(2020, 6, 12, 12, 22, 16, 0, DateTimeKind.Unspecified), null, false, null, new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55"), "https://www.youtube.com/embed/GU5jderTllw" }
                });

            migrationBuilder.InsertData(
                table: "UserFriends",
                columns: new[] { "Id", "FriendId", "UserId" },
                values: new object[,]
                {
                    { 1, new Guid("b6de00f9-b6ac-497b-9ba2-6df1b38a8f24"), new Guid("51d2ad1a-2254-4099-891b-00f854a9467a") },
                    { 3, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55") },
                    { 2, new Guid("0fdd5513-78ca-46de-853e-50cea86e5db1"), new Guid("51d2ad1a-2254-4099-891b-00f854a9467a") },
                    { 4, new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55"), new Guid("0fdd5513-78ca-46de-853e-50cea86e5db1") }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "CreatedOn", "DeletedOn", "IsDeleted", "PostId", "UserId" },
                values: new object[,]
                {
                    { 5, "Roberto carlos pass is awesome!", new DateTime(2020, 11, 26, 14, 53, 16, 0, DateTimeKind.Unspecified), null, false, 1, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a") },
                    { 1, "This really makes me want to explore the world even more", new DateTime(2020, 11, 26, 14, 40, 16, 0, DateTimeKind.Unspecified), null, false, 8, new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55") },
                    { 3, "Absolutely breathtaking scenes!", new DateTime(2020, 11, 26, 14, 43, 16, 0, DateTimeKind.Unspecified), null, false, 8, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a") },
                    { 4, "This is just amazing!", new DateTime(2020, 11, 26, 14, 45, 16, 0, DateTimeKind.Unspecified), null, false, 8, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a") },
                    { 6, "Amazing Nature,Beautiful Music,thank You!", new DateTime(2020, 11, 30, 14, 45, 16, 0, DateTimeKind.Unspecified), null, false, 8, new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55") }
                });

            migrationBuilder.InsertData(
                table: "Likes",
                columns: new[] { "PostId", "UserId", "LikeType" },
                values: new object[,]
                {
                    { 4, new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55"), 1 },
                    { 1, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), 1 },
                    { 8, new Guid("5538382a-5cbb-4d03-8840-61b245b7ab55"), 1 },
                    { 8, new Guid("51d2ad1a-2254-4099-891b-00f854a9467a"), 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_PhotoId",
                table: "AspNetUsers",
                column: "PhotoId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserId",
                table: "Comments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_FriendRequests_SenderId",
                table: "FriendRequests",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Likes_UserId",
                table: "Likes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_PhotoId",
                table: "Posts",
                column: "PhotoId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_UserId",
                table: "Posts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFriends_FriendId",
                table: "UserFriends",
                column: "FriendId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFriends_UserId",
                table: "UserFriends",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "FriendRequests");

            migrationBuilder.DropTable(
                name: "Likes");

            migrationBuilder.DropTable(
                name: "UserFriends");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Photos");
        }
    }
}
