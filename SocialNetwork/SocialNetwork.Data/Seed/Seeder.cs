﻿namespace SocialNetwork.Data.Seed
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Data.Models.Enums;

    public static class Seeder
    {
        public static void Seed(this ModelBuilder builder)
        {
            builder.Entity<Role>()
                .HasData(
                new Role() { Id = Guid.Parse("50ee8e13-16ce-4168-8c30-13e244c4497e"), Name = "Admin", NormalizedName = "ADMIN" },
                new Role() { Id = Guid.Parse("177513b2-a3fb-4c13-a481-39bff26141c3"), Name = "User", NormalizedName = "USER" }
                );

            var photo = new List<Photo>
            {
                new Photo
                {
                    Id= 1,
                    PhotoName = "profilePicture"
                },
                new Photo
                {
                    Id= 2,
                    PhotoName = "night20374837.jpg"
                },
                new Photo
                {
                    Id= 3,
                    PhotoName = "2020502529.jpg"
                },
                new Photo
                {
                    Id= 4,
                    PhotoName = "3620514289.jpg"
                },
                new Photo
                {
                    Id= 5,
                    PhotoName = "photo_large20554592"
                },
                new Photo
                {
                    Id= 6,
                    PhotoName = "9420511100.jpg"
                },
                new Photo
                {
                    Id= 7,
                    PhotoName = "me20185292.jpg"
                },
                new Photo
                {
                    Id= 8,
                    PhotoName = "nathanHill20143094.jpg"
                },
                new Photo
                {
                    Id= 9,
                    PhotoName = "antonia20163919.jpg"
                },
                new Photo
                {
                    Id= 10,
                    PhotoName = "dolpin20570548.jpeg"
                },
                new Photo
                {
                    Id= 11,
                    PhotoName = "nightsky20082107.jpg"
                },
                new Photo
                {
                    Id= 12,
                    PhotoName = "savanah20183396.jpg"
                },
                new Photo
                {
                    Id= 13,
                    PhotoName = "seal20300244.jpg"
                }
            };
            builder.Entity<Photo>().HasData(photo);

            var likes = new List<Like>
            {
                new Like
                {
                    UserId = Guid.Parse("5538382A-5CBB-4D03-8840-61B245B7AB55"),
                    PostId = 8,
                    LikeType = LikeStatus.UpLike
                },
                new Like
                {
                    UserId = Guid.Parse("51D2AD1A-2254-4099-891B-00F854A9467A"),
                    PostId = 8,
                    LikeType = LikeStatus.UpLike
                },
                new Like
                {
                    PostId = 4,
                    UserId = Guid.Parse("5538382A-5CBB-4D03-8840-61B245B7AB55"),
                    LikeType = LikeStatus.UpLike
                },
                new Like
                {
                    PostId = 1,
                    UserId = Guid.Parse("51D2AD1A-2254-4099-891B-00F854A9467A"),
                    LikeType = LikeStatus.UpLike
                }
            };
            builder.Entity<Like>().HasData(likes);

            var comments = new List<Comment>
            {
                new Comment
                {
                    Id=1,
                    Content="This really makes me want to explore the world even more",
                    CreatedOn = DateTime.Parse("2020-11-26 14:40:16"),
                    PostId = 8,
                    UserId = Guid.Parse("5538382A-5CBB-4D03-8840-61B245B7AB55")
                },
                new Comment
                {
                    Id=3,
                    Content="Absolutely breathtaking scenes!",
                    CreatedOn = DateTime.Parse("2020-11-26 14:43:16"),
                    PostId = 8,
                    UserId = Guid.Parse("51D2AD1A-2254-4099-891B-00F854A9467A")
                },
                new Comment
                {
                    Id=5,
                    Content="Roberto carlos pass is awesome!",
                    CreatedOn = DateTime.Parse("2020-11-26 14:53:16"),
                    PostId = 1,
                    UserId = Guid.Parse("51D2AD1A-2254-4099-891B-00F854A9467A")
                },
                new Comment
                {
                    Id=4,
                    Content="This is just amazing!",
                    CreatedOn = DateTime.Parse("2020-11-26 14:45:16"),
                    PostId = 8,
                    UserId = Guid.Parse("51D2AD1A-2254-4099-891B-00F854A9467A")
                },
                new Comment
                {
                    Id=6,
                    Content="Amazing Nature,Beautiful Music,thank You!",
                    CreatedOn = DateTime.Parse("2020-11-30 14:45:16"),
                    PostId = 8,
                    UserId = Guid.Parse("5538382A-5CBB-4D03-8840-61B245B7AB55")
                },
                new Comment
                {
                    Id=7,
                    Content="Extraordinary sea life",
                    CreatedOn = DateTime.Parse("2020-10-26 11:14:16"),
                    PostId = 11,
                    UserId = Guid.Parse("4922B615-00AD-4DED-B733-FAC0E52B6463")
                },
            };
            builder.Entity<Comment>().HasData(comments);

            var posts = new List<Post>
            {
                new Post
                {
                    Id= 1,
                    Content = "Legendary goals",
                    CreatedOn = DateTime.Parse("12 June 2020 12:22:16"),
                    VideoUrl ="https://www.youtube.com/embed/GU5jderTllw",
                    AccessStatus = AccessStatus.Public,
                    UserId = Guid.Parse("5538382a-5cbb-4d03-8840-61b245b7ab55")
                },
                new Post
                {
                    Id= 10,
                    CreatedOn = DateTime.Parse("12 November 2020 07:22:16"),
                    Content = "Dolphin tricks",
                    PhotoId = 11,
                    AccessStatus = AccessStatus.Private,
                    UserId = Guid.Parse("4922B615-00AD-4DED-B733-FAC0E52B6463")
                },
                new Post
                {
                    Id= 11,
                    Content = "Sea Life Oberhausen",
                    CreatedOn = DateTime.Parse("17 November 2020 07:50:16"),
                    VideoUrl ="https://www.youtube.com/embed/_EaMFf97Ab8",
                    AccessStatus = AccessStatus.Public,
                    UserId = Guid.Parse("4922B615-00AD-4DED-B733-FAC0E52B6463")
                },
                new Post
                {
                    Id= 2,
                    Content = "Thunder struck",
                    CreatedOn = DateTime.Parse("17 November 2020 17:50:16"),
                    PhotoId = 11,
                    AccessStatus = AccessStatus.Private,
                    UserId =Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a")
                },
                new Post
                {
                    Id= 3,
                    Content = "From Earth To the Milky Way Galaxy",
                    CreatedOn = DateTime.Parse("17 November 2020 13:05:12"),
                    VideoUrl = "https://www.youtube.com/embed/0La58bUNNzQ",
                    AccessStatus = AccessStatus.Public,
                    UserId = Guid.Parse("2D0760D0-82C2-47C4-B5A1-5924D69CA5DB")
                },
                new Post
                {
                    Id= 4,
                    Content = "Night Sky",
                    CreatedOn = DateTime.Parse("17 september 2020 13:05:12"),
                    VideoUrl = "https://www.youtube.com/embed/jfVpwHrNzMA",
                    AccessStatus = AccessStatus.Public,
                    UserId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a")
                },
                new Post
                {
                    Id= 5,
                    Content = "Savannah",
                    CreatedOn = DateTime.Parse("17 October 2020 13:05:12"),
                    PhotoId = 12,
                    AccessStatus = AccessStatus.Public,
                    UserId = Guid.Parse("5538382A-5CBB-4D03-8840-61B245B7AB55")
                },
                new Post
                {
                    Id= 6,
                    Content = "Four Seasons",
                    CreatedOn = DateTime.Parse("27 November 2020 13:05:12"),
                    VideoUrl = "https://www.youtube.com/embed/GRxofEmo3HA",
                    AccessStatus = AccessStatus.Private,
                    UserId = Guid.Parse("202AC5CD-AFA2-40F7-BBB6-416FDDCBCC2B")
                },
                new Post
                {
                    Id= 7,
                    Content = "Top 20 Best Football Players of All Time",
                    CreatedOn = DateTime.Parse("27 November 2020 15:05:12"),
                    VideoUrl ="https://www.youtube.com/embed/rA1102ZzprY",
                    AccessStatus = AccessStatus.Private,
                    UserId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a")
                },
                new Post
                {
                    Id= 8,
                    Content = "The Breathtaking Beauty of Nature",
                    CreatedOn = DateTime.Parse("22 November 2020 11:15:12"),
                    VideoUrl ="https://www.youtube.com/embed/IUN664s7N-c",
                    AccessStatus = AccessStatus.Public,
                    UserId = Guid.Parse("0FDD5513-78CA-46DE-853E-50CEA86E5DB1")
                },
                new Post
                {
                    Id= 9,
                    Content = "Time for a nap",
                    CreatedOn = DateTime.Parse("17 November 2020 13:05:12"),
                    PhotoId = 13,
                    AccessStatus = AccessStatus.Public,
                    UserId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a")
                },
            };
            builder.Entity<Post>().HasData(posts);
            //TODO Da se updatnat userite
            //admin
            var hasher = new PasswordHasher<User>();
            var adminUser = new User();
            adminUser.Id = Guid.Parse("0fdd5513-78ca-46de-853e-50cea86e5db1");
            adminUser.FirstName = "Georgi";
            adminUser.LastName = "Georgiev";
            adminUser.UserName = "ggeorgiev@gmail.com";
            adminUser.NormalizedUserName = "GGEORGIEV@GMAIL.COM";
            adminUser.PhotoId = 6;
            adminUser.Gender = Gender.Male;
            adminUser.Country = "Bulgaria";
            adminUser.EmailConfirmed = true;
            adminUser.PhotoName = "9420511100.jpg";
            adminUser.Email = "ggeorgiev@gmail.com";
            adminUser.DateOfBirth = DateTime.Parse("12 June 1988");
            adminUser.NormalizedEmail = "GGEORGIEV@GMAIL.COM";
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "georgi123");
            builder.Entity<User>().HasData(adminUser);
            builder.Entity<IdentityUserRole<Guid>>()
                .HasData(
                new IdentityUserRole<Guid>()
                {
                    RoleId = Guid.Parse("50ee8e13-16ce-4168-8c30-13e244c4497e"),
                    UserId = Guid.Parse("0fdd5513-78ca-46de-853e-50cea86e5db1")
                });

            //mariyan
            adminUser = new User();
            adminUser.Id = Guid.Parse("202AC5CD-AFA2-40F7-BBB6-416FDDCBCC2B");
            adminUser.FirstName = "Mariyan";
            adminUser.LastName = "Bogomilov";
            adminUser.IsPrivate = true;
            adminUser.UserName = "mariyan@gmail.com";
            adminUser.NormalizedUserName = "MARIYAN@GMAIL.COM";
            adminUser.PhotoId = 7;
            adminUser.EmailConfirmed = true;
            adminUser.Gender = Gender.Male;
            adminUser.Country = "Bulgaria";
            adminUser.DateOfBirth = DateTime.Parse("20 July 1994");
            adminUser.PhotoName = "me20185292.jpg";
            adminUser.Email = "mariyan@gmail.com";
            adminUser.NormalizedEmail = "MARIYAN@GMAIL.COM";
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "mariyan123");
            builder.Entity<User>().HasData(adminUser);
            builder.Entity<IdentityUserRole<Guid>>()
                .HasData(
                new IdentityUserRole<Guid>()
                {
                    RoleId = Guid.Parse("50ee8e13-16ce-4168-8c30-13e244c4497e"),
                    UserId = Guid.Parse("202AC5CD-AFA2-40F7-BBB6-416FDDCBCC2B")
                });
            //alice
            var regularUser = new User();
            regularUser.Id = Guid.Parse("5538382a-5cbb-4d03-8840-61b245b7ab55");
            regularUser.FirstName = "Alice";
            regularUser.LastName = "Dimitrova";
            regularUser.UserName = "alice@gmail.com";
            regularUser.NormalizedUserName = "ALICE@GMAIL.COM";
            regularUser.PhotoId = 5;
            regularUser.Gender = Gender.Female;
            regularUser.Country = "Bulgaria";
            regularUser.EmailConfirmed = true;
            regularUser.DateOfBirth = DateTime.Parse("20 August 1990");
            regularUser.PhotoName = "photo_large20554592.jpg";
            regularUser.Email = "alice@gmail.com";
            regularUser.NormalizedEmail = "ALICE@GMAIL.COM";
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "alice123");
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>()
                {
                    RoleId = Guid.Parse("177513b2-a3fb-4c13-a481-39bff26141c3"),
                    UserId = Guid.Parse("5538382a-5cbb-4d03-8840-61b245b7ab55")
                }
            );
            // Bob
            regularUser = new User();
            regularUser.Id = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a");
            regularUser.FirstName = "Bob";
            regularUser.LastName = "Borimirov";
            regularUser.PhotoId = 4;
            regularUser.EmailConfirmed = true;
            regularUser.Gender = Gender.Male;
            regularUser.PhotoName = "3620514289.jpg";
            regularUser.UserName = "borimirov@gmail.com";
            regularUser.NormalizedUserName = "BORIMIROV@GMAIL.COM";
            regularUser.Email = "borimirov@gmail.com";
            regularUser.Country = "Bulgaria";
            regularUser.DateOfBirth = DateTime.Parse("1 January 1970");
            regularUser.NormalizedEmail = "BORIMIROV@GMAIL.COM";
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "bob123");
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>()
                {
                    RoleId = Guid.Parse("177513b2-a3fb-4c13-a481-39bff26141c3"),
                    UserId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a")
                }
            );

            regularUser = new User();
            regularUser.Id = Guid.Parse("b6de00f9-b6ac-497b-9ba2-6df1b38a8f24");
            regularUser.FirstName = "Georgi";
            regularUser.LastName = "Ivanov";
            regularUser.PhotoId = 3;
            regularUser.EmailConfirmed = true;
            regularUser.Gender = Gender.Male;
            regularUser.UserName = "goshi@gmail.com";
            regularUser.PhotoName = "2020502529.jpg";
            regularUser.NormalizedUserName = "GOSHI@GMAIL.COM";
            regularUser.Country = "Bulgaria";
            regularUser.DateOfBirth = DateTime.Parse("12 February 1982");
            regularUser.Email = "goshi@gmail.com";
            regularUser.NormalizedEmail = "GOSHI@GMAIL.COM";
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "gosho123");
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>()
                {
                    RoleId = Guid.Parse("177513b2-a3fb-4c13-a481-39bff26141c3"),
                    UserId = Guid.Parse("b6de00f9-b6ac-497b-9ba2-6df1b38a8f24")
                }
            );

            regularUser = new User();
            regularUser.Id = Guid.Parse("4922B615-00AD-4DED-B733-FAC0E52B6463");
            regularUser.FirstName = "Antonia";
            regularUser.EmailConfirmed = true;
            regularUser.LastName = "Dimitrova";
            regularUser.Gender = Gender.Female;
            regularUser.UserName = "antonia@mail.com";
            regularUser.PhotoName = "antonia20163919.jpg";
            regularUser.NormalizedUserName = "ANTONIA@MAIL.COM";
            regularUser.DateOfBirth = DateTime.Parse("12 September 1998");
            regularUser.Email = "antonia@mail.com";
            regularUser.Country = "Bulgaria";
            regularUser.NormalizedEmail = "ANTONIA@MAIL.COM";
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "antonia123");
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>()
                {
                    RoleId = Guid.Parse("177513b2-a3fb-4c13-a481-39bff26141c3"),
                    UserId = Guid.Parse("4922B615-00AD-4DED-B733-FAC0E52B6463")
                }
            );

            regularUser = new User();
            regularUser.Id = Guid.Parse("2D0760D0-82C2-47C4-B5A1-5924D69CA5DB");
            regularUser.FirstName = "Nathan";
            regularUser.LastName = "Hill";
            regularUser.EmailConfirmed = true;
            regularUser.Gender = Gender.Male;
            regularUser.UserName = "nathanhill@mail.com";
            regularUser.PhotoName = "nathanHill20143094.jpg";
            regularUser.Country = "France";
            regularUser.NormalizedUserName = "NATHANHILL@MAIL.COM";
            regularUser.DateOfBirth = DateTime.Parse("12 May 1978");
            regularUser.Email = "nathanhill@mail.com";
            regularUser.NormalizedEmail = "NATHANHILL@MAIL.COM";
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "nathan123");
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>()
                {
                    RoleId = Guid.Parse("177513b2-a3fb-4c13-a481-39bff26141c3"),
                    UserId = Guid.Parse("2D0760D0-82C2-47C4-B5A1-5924D69CA5DB")
                }
            );

            var userFriends = new List<UserFriend>
            {
                new UserFriend
                {
                    Id = 1,
                    UserId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a"),
                    FriendId = Guid.Parse("b6de00f9-b6ac-497b-9ba2-6df1b38a8f24")
                },
                new UserFriend
                {
                    Id = 2,
                    UserId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a"),
                    FriendId = Guid.Parse("0fdd5513-78ca-46de-853e-50cea86e5db1")
                },
                new UserFriend
                {
                    Id = 3,
                    UserId = Guid.Parse("5538382a-5cbb-4d03-8840-61b245b7ab55"),
                    FriendId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a")
                },
                new UserFriend
                {
                    Id = 4,
                    UserId = Guid.Parse("0fdd5513-78ca-46de-853e-50cea86e5db1"),
                    FriendId = Guid.Parse("5538382a-5cbb-4d03-8840-61b245b7ab55")
                }
            };
            builder.Entity<UserFriend>().HasData(userFriends);

            var friendRequests = new List<FriendRequest>
            {
                new FriendRequest
                {
                    SenderId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a"),
                    ReceiverId = Guid.Parse("b6de00f9-b6ac-497b-9ba2-6df1b38a8f24"),
                    FriendRequestStatus = FriendRequestStatus.Accepted
                },
                new FriendRequest
                {
                    SenderId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a"),
                    ReceiverId = Guid.Parse("0fdd5513-78ca-46de-853e-50cea86e5db1"),
                    FriendRequestStatus = FriendRequestStatus.Accepted
                },
                new FriendRequest
                {
                    SenderId = Guid.Parse("5538382a-5cbb-4d03-8840-61b245b7ab55"),
                    ReceiverId = Guid.Parse("51d2ad1a-2254-4099-891b-00f854a9467a"),
                    FriendRequestStatus = FriendRequestStatus.Accepted
                },
                new FriendRequest
                {
                    SenderId = Guid.Parse("0fdd5513-78ca-46de-853e-50cea86e5db1"),
                    ReceiverId = Guid.Parse("5538382a-5cbb-4d03-8840-61b245b7ab55"),
                    FriendRequestStatus = FriendRequestStatus.Accepted
                }
            };
            builder.Entity<FriendRequest>().HasData(friendRequests);
        }
    }
}
