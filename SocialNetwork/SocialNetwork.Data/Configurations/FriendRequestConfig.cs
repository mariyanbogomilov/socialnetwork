﻿namespace SocialNetwork.Data.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Models;

    public class FriendRequestConfig : IEntityTypeConfiguration<FriendRequest>
    {
        public void Configure(EntityTypeBuilder<FriendRequest> builder)
        {
            builder.HasKey(f => new { f.ReceiverId, f.SenderId });

            builder
                .HasOne(f => f.Receiver)
                .WithMany(f => f.FriendRequestReceived)
                .HasForeignKey(c => c.ReceiverId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(f => f.Sender)
                .WithMany(f => f.FriendRequestSent)
                .HasForeignKey(c => c.SenderId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}

