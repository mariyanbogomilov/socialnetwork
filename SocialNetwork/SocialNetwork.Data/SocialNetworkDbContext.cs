﻿namespace SocialNetwork.Data
{
    using System;
    using Configurations;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using SocialNetwork.Data.Seed;

    public class SocialNetworkDbContext : IdentityDbContext<User, Role, Guid>
    {
        public SocialNetworkDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Like> Likes { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<UserFriend> UserFriends { get; set; }

        public DbSet<Photo> Photos { get; set; }

        public DbSet<FriendRequest> FriendRequests { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new PostConfig());
            builder.ApplyConfiguration(new CommentConfig());
            builder.ApplyConfiguration(new LikeConfig());
            builder.ApplyConfiguration(new UserFriendConfig());
            builder.ApplyConfiguration(new FriendRequestConfig());
            builder.ApplyConfiguration(new PhotoConfig());

            builder.Seed();
            base.OnModelCreating(builder);
        }
    }
}