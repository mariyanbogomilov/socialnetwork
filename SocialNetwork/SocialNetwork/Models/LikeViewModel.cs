﻿using SocialNetwork.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Models
{
    public class LikeViewModel
    {
        public int PostId { get; set; }
        public Guid UserId { get; set; }
        public LikeStatus LikeType { get; set; }
    }
}
