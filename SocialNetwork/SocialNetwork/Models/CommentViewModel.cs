﻿using SocialNetwork.Web.Models.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Models
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(500, MinimumLength = 6, ErrorMessage = "Invalid comment content")]
        public string Content { get; set; }
        public int PostId { get; set; }
        public Guid UserId { get; set; }
        public string Username { get; set; }

        public virtual UserViewModel User { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
