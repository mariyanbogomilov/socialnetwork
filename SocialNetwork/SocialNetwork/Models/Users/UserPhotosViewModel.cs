﻿namespace SocialNetwork.Web.Models.Users
{
    using System;
    using System.Collections.Generic;

    public class UserPhotosViewModel
    {
        public Guid Id { get; set; }
        public IEnumerable<string> Photos { get; set; }
    }
}
