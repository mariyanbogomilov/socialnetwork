﻿namespace SocialNetwork.Web.Models.Users
{
    using Microsoft.AspNetCore.Http;
    using SocialNetwork.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.Json.Serialization;

    public class UserViewModel
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhotoName { get; set; }
        public string Gender { get; set; }
        public string Country { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string JobTitle { get; set; }
        public bool IsPrivate { get; set; }
        public string Bio { get; set; }
        [JsonIgnore]
        public PhotoViewModel ProfilePicture { get; set; }
        [JsonIgnore]
        public IFormFile PhotoFile { get; set; }
        [JsonIgnore]
        public ICollection<PostViewModel> Posts { get; set; }
        public string FullName => $"{this.FirstName} {this.LastName}";
        [JsonIgnore]
        public IEnumerable<UserViewModel> Friends { get; set; }
        public int FriendsCount { get; set; }
        public int Age => (DateTime.Today.Year - DateOfBirth.Year);
        [JsonIgnore]
        public DateTime CreatedOn { get; set; }
        [JsonIgnore]
        public string CreatedDate => $"{this.CreatedOn.Day} {this.CreatedOn.ToString("MMM", CultureInfo.InvariantCulture)} {this.CreatedOn.Year}";
        [JsonIgnore]
        public bool AreFriends { get; set; }
        [JsonIgnore]
        public bool IsRequestSent { get; set; }
    }
}
