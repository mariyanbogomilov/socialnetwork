﻿namespace SocialNetwork.Web.Models.Users
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Microsoft.AspNetCore.Http;

    public class UserInputViewModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [EmailAddress]
        public string Email { get; set; }
        [StringLength(375, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string Bio { get; set; }
        [StringLength(375, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string JobTitle { get; set; }
        public bool IsPrivate { get; set; }
        public string PhotoName { get; set; }
        public string Country { get; set; }
        public DateTime DateOfBirth { get; set; }
        public IFormFile PhotoFile { get; set; }
        public string DateOfBirthView => DateOfBirth.ToShortDateString();

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$", ErrorMessage = "Password must be at least 6 characters and should contain at least 1 letter and 1 number")]
        public string CurrentPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$", ErrorMessage = "Password must be at least 6 characters and should contain at least 1 letter and 1 number")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessage = "The passwords does not match.")]
        public string RepeatNewPassword { get; set; }
    }
}
