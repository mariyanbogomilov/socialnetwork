﻿using Microsoft.AspNetCore.Http;
using SocialNetwork.Data.Models;
using SocialNetwork.Data.Models.Enums;
using SocialNetwork.Web.Models.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }

        [JsonIgnore]
        public AccessStatus AccessStatus { get; set; }

        [JsonIgnore]
        public int? PhotoId { get; set; }
        public string PhotoName { get; set; }
        public string VideoUrl { get; set; }
        public string Username { get; set; }

        [JsonIgnore]
        public UserViewModel User { get; set; }

        [JsonIgnore]
        public Guid UserId { get; set; }

        [NotMapped]
        [JsonIgnore]
        public IFormFile PhotoFile { get; set; }

        [NotMapped]
        [JsonIgnore]
        public int Count { get; set; }

        [JsonIgnore]
        public int CommentsCount { get; set; }

        [JsonIgnore]
        public virtual ICollection<CommentViewModel> Comments { get; set; }

        [JsonIgnore]
        public virtual ICollection<LikeViewModel> Likes { get; set; }
        public DateTime CreatedOn { get; set; }
        //public bool IsDeleted { get; set; }
        //public DateTime? DeletedOn { get; set; }
    }
}
