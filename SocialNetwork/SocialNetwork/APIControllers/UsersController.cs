﻿namespace SocialNetwork.Web.APIControllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.UserService;
    using System;
    using System.Threading.Tasks;
    using System.Linq;
    using SocialNetwork.Web.Models.Users;
    using Microsoft.AspNetCore.Authorization;

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private const int FriendsPerPage = 1;

        private readonly IUserService userService;
        private readonly IMapper mapper;
        private readonly UserManager<User> userManager;

        public UsersController(IUserService userService, IMapper mapper, UserManager<User> userManager)
        {
            this.userService = userService;
            this.mapper = mapper;
            this.userManager = userManager;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetUser(Guid id)
        {
            try
            {
                var user = await this.userService.GetUser(id);
                var userToViewModel = this.mapper.Map<UserViewModel>(user);

                return Ok(userToViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("friends")]
        [Authorize]
        public async Task<IActionResult> GetUserFriends(int page = 1)
        {
            var userId = Guid.Parse(this.userManager.GetUserId(this.User));

            var friends = await this.userService.GetAllFriends(userId, FriendsPerPage, (page - 1) * FriendsPerPage);

            var count = this.userService.GetFriendsCount(userId, Guid.Empty);

            var friendsViewModel = new FriendsViewModel
            {
                Friends = friends.Select(u => this.mapper.Map<UserViewModel>(u)),
                FriendsPagesCount = (int)Math.Ceiling((double)count / FriendsPerPage)
            };

            if (friendsViewModel.FriendsPagesCount == 0)
            {
                friendsViewModel.FriendsPagesCount = 1;
            }

            friendsViewModel.CurrentPage = page;

            return Ok(friendsViewModel);
        }

    }
}
