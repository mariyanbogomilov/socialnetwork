﻿namespace SocialNetwork.Web.APIControllers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.PostService.Contract;
    using SocialNetwork.Services.UserService;
    using SocialNetwork.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostsController : ControllerBase
    {
        private readonly IPostService postService;
        private readonly IUserService userService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> userManager;
        public PostsController(IPostService postService, IMapper mapper, UserManager<User> userManager, IUserService userService)
        {
            this.postService = postService;
            this._mapper = mapper;
            this.userManager = userManager;
            this.userService = userService;
        }

        //[ApiExplorerSettings(IgnoreApi = true)]
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<PostViewModel>>> GetPosts()
        //{
        //    var posts = await this.postService.GetAllPosts();
        //    var post = _mapper.Map<List<PostViewModel>>(posts);
        //    return post;
        //}

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("/ownPosts")]
        public async Task<ActionResult<IEnumerable<PostViewModel>>> GetPostsForUsers(Guid id)
        {
            var posts = await this.postService.GetPostsForUsers(id);
            var post = _mapper.Map<List<PostViewModel>>(posts);
            return post;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("{id}")]
        public async Task<ActionResult<PostViewModel>> GetPost(int id)
        {
            var post = await postService.GetPost(id);

            if (post == null)
            {
                return NotFound();
            }

            return _mapper.Map<PostViewModel>(post);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPost(int id, [FromBody] PostViewModel post)
        {
            if (post == null)
            {
                return BadRequest();
            }
            var postDTO = this._mapper.Map<PostDTO>(post);
        
            var updatedPost = await this.postService.UpdatePost(id, postDTO);
        
            return Ok(updatedPost);
        }

        [HttpGet("/api/newsfeed")]
        public async Task<ActionResult<IEnumerable<PostViewModel>>> NewsfeedPosts()
        {
            var user = await this.userManager.GetUserAsync(this.User);
            var friends = await this.userService.GetAllFriends(user.Id);
            var p = new List<PostDTO>();
            foreach (var friend in friends)
            {
                foreach (var item in await this.postService.GetPostsForUsers(friend.Id))
                {
                    p.Add(item);
                }
            }
            var posts = await this.postService.GetPostsForUsers(user.Id);
            p.AddRange(posts);
            var map = _mapper.Map<List<PostViewModel>>(p.OrderByDescending(p => p.CreatedOn));
          
            return map;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<PostViewModel>> PostPost([FromBody] PostViewModel post)
        {
            var user = await this.userManager.GetUserAsync(this.User);
            post.UserId = user.Id;
            var postDTO = this._mapper.Map<PostDTO>(post);
            var postMod = await this.postService.AddPost(postDTO);

            return Created("post", postMod);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("delete")]
        public async Task<ActionResult> DeletePost([FromQuery] int id)
        {
            var result = await postService.DeletePost(id);
            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
