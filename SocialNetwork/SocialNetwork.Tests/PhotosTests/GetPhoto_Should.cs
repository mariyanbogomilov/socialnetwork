﻿namespace SocialNetwork.Tests.PhotosTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PhotoService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.UserService;
    using System;

    [TestClass]
    public class GetPhoto_Should
    {
        [TestMethod]
        public void GetPhoto_Success()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetPhoto_Success));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var photoDTO = new PhotoDTO
            {
                Id = 3,
                PhotoName = "testPhoto.png"
            };

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PhotoService(assertContext, mapper);
                var addPhoto = sut.AddPhoto(photoDTO);
                var actual = sut.GetPhoto(3).Result;


                Assert.AreEqual(photoDTO.Id, actual.Id);
                Assert.AreEqual(photoDTO.PhotoName, actual.PhotoName);
            }
        }
    }
}
