﻿namespace SocialNetwork.Tests.PhotosTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PhotoService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.UserService;
    using System;

    [TestClass]
    public class DeletePhoto_Should
    {
        [TestMethod]
        public void DeletePhoto_Success()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(DeletePhoto_Success));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var photoDTO = new PhotoDTO
            {
                Id = 2,
                PhotoName = "testPhoto.png"
            };

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PhotoService(assertContext, mapper);
                var addPhoto = sut.AddPhoto(photoDTO);
                var actual = sut.DeletePhoto(2).Result;


                Assert.AreEqual(true, actual);
            }
        }
        [TestMethod]
        public void DeletePhoto_Fail()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(DeletePhoto_Fail));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();


            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PhotoService(assertContext, mapper);

                var actual = sut.DeletePhoto(4).Result;


                Assert.AreEqual(false, actual);
            }
        }
    }
}
