﻿namespace SocialNetwork.Tests.UserFriendTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Services.UserService;
    using System.Linq;

    [TestClass]
    public class AddFriend_Should
    {
        [TestMethod]
        public void AddFriend_When_ParamsAreValid()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(AddFriend_When_ParamsAreValid));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();

            var mockUserService = new Mock<IUserService>();

            using(var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserFriendService(assertContext, mockUserService.Object, mapper);

                var act = sut.AddFriend(firstUser.Id, secondUser.Id);

                var contextResult = assertContext.UserFriends.First();

                Assert.AreEqual(firstUser.Id, contextResult.UserId);
                Assert.AreEqual(secondUser.Id, contextResult.FriendId);
            }
        }
    }
}
