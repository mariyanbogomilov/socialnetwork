﻿namespace SocialNetwork.Tests.UserFriendTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Services.UserService;
    using System.Linq;

    [TestClass]
    public class RemoveFriend_Should
    {
        [TestMethod]
        public void ReturnTrue_When_ParamsAreValid()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnTrue_When_ParamsAreValid));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var userFriend = SocialNetworkUtility.GetUserFriends().First();
            var friendRequest = SocialNetworkUtility.GetAcceptedFriendRequest().First();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(u => u.Exists(firstUser.Id)).Returns(true);
            mockUserService.Setup(u => u.Exists(secondUser.Id)).Returns(true);
            mockUserService.Setup(u => u.CheckIfFriends(firstUser.Id, secondUser.Id)).ReturnsAsync(true);

            using(var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.UserFriends.Add(userFriend);
                arrangeContext.FriendRequests.Add(friendRequest);
                arrangeContext.SaveChanges();
            }

            using(var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserFriendService(assertContext, mockUserService.Object, mapper);

                var act = sut.RemoveFriend(firstUser.Id, secondUser.Id).Result;

                var contextUserFriendResult = assertContext.UserFriends.Count();
                var contextFriendRequestResult = assertContext.FriendRequests.Count();

                Assert.AreEqual(0, contextUserFriendResult);
                Assert.AreEqual(0, contextFriendRequestResult);
                Assert.AreEqual(true, act);
            }
        }

        [TestMethod]
        public void ReturnFalse_When_UserDoesNotExist()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnFalse_When_UserDoesNotExist));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(u => u.Exists(firstUser.Id)).Returns(true);
            mockUserService.Setup(u => u.Exists(secondUser.Id)).Returns(false);

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserFriendService(assertContext, mockUserService.Object, mapper);

                var act = sut.RemoveFriend(firstUser.Id, secondUser.Id).Result;

                Assert.AreEqual(false, act);
            }
        }

        [TestMethod]
        public void ReturnFalse_When_TheyAreNotFriends()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnFalse_When_TheyAreNotFriends));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var pendingFriendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(u => u.Exists(firstUser.Id)).Returns(true);
            mockUserService.Setup(u => u.Exists(secondUser.Id)).Returns(true);
            mockUserService.Setup(u => u.CheckIfFriends(firstUser.Id, secondUser.Id)).ReturnsAsync(false);

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.FriendRequests.Add(pendingFriendRequest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserFriendService(assertContext, mockUserService.Object, mapper);

                var act = sut.RemoveFriend(firstUser.Id, secondUser.Id).Result;

                Assert.AreEqual(false, act);
            }
        }
    }
}
