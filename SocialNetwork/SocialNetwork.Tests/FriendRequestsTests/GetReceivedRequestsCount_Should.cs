﻿namespace SocialNetwork.Tests.FriendRequestsTests
{
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Services.UserFriend;
    using Services.UserService;
    using System.Linq;
    using Services.FriendRequestService;

    [TestClass]
    public class GetReceivedRequestsCount_Should
    {
        [TestMethod]
        public void ReturnCorrectReceivedRequestsCount()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnCorrectReceivedRequestsCount));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(2).First();
            var firstFriendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();
            var secondFriendRequest = SocialNetworkUtility.GetPendingFriendRequest().Skip(1).First();

            var mockUserService = new Mock<IUserService>();

            var mockUserFriendService = new Mock<IUserFriendService>();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Users.Add(thirdUser);
                arrangeContext.FriendRequests.Add(firstFriendRequest);
                arrangeContext.FriendRequests.Add(secondFriendRequest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                var act = sut.GetReceivedRequestsCount(secondUser.Id);

                Assert.AreEqual(2, act);
            }
        }
    }
}
