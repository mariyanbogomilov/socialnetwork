﻿namespace SocialNetwork.Tests.FriendRequestsTests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.FriendRequestService;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Services.UserService;
    using System.Linq;

    [TestClass]
    public class UserSentRequest_Should
    {
        [TestMethod]
        public void ReturnCorrectSentRequestsCount()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnCorrectSentRequestsCount));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(2).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            var secondFriendRequest = new FriendRequest
            {
                SenderId = firstUser.Id,
                ReceiverId = thirdUser.Id
            };

            var mockUserService = new Mock<IUserService>();
            var mockUserFriendService = new Mock<IUserFriendService>();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(thirdUser);
                arrangeContext.FriendRequests.Add(friendRequest);
                arrangeContext.FriendRequests.Add(secondFriendRequest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                var act = sut.UserSentRequests(firstUser.Id);

                Assert.AreEqual(2, act.Result.Count());
            }
        }

        [TestMethod]
        public void Throw_When_UserIsNull()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Throw_When_UserIsNull));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();

            var mockUserService = new Mock<IUserService>();
            var mockUserFriendService = new Mock<IUserFriendService>();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UserSentRequests(firstUser.Id));
            }
        }
    }
}
