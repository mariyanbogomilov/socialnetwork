﻿namespace SocialNetwork.Tests.FriendRequestsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models.Enums;
    using SocialNetwork.Services.FriendRequestService;
    using SocialNetwork.Services.UserFriend;
    using SocialNetwork.Services.UserService;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    [TestClass]
    public class SendFriendRequest_Should
    {
        [TestMethod]
        public void ReturnFriendRequestsCorrectCount_When_SendingRequest()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnFriendRequestsCorrectCount_When_SendingRequest));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(u => u.Exists(firstUser.Id)).Returns(true);
            mockUserService.Setup(u => u.Exists(secondUser.Id)).Returns(true);

            var mockUserFriendService = new Mock<IUserFriendService>();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                var act = sut.SendFriendRequest(firstUser.Id, secondUser.Id);

                Assert.AreEqual(1, assertContext.FriendRequests.Count());
                Assert.AreEqual(FriendRequestStatus.Pending, assertContext.FriendRequests.First().FriendRequestStatus);
                Assert.AreEqual(friendRequest.SenderId, assertContext.FriendRequests.First().SenderId);
                Assert.AreEqual(friendRequest.ReceiverId, assertContext.FriendRequests.First().ReceiverId);
            }
        }

        [TestMethod]
        public void Throw_When_FriendRequestAlreadyExists()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Throw_When_FriendRequestAlreadyExists));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(u => u.Exists(firstUser.Id)).Returns(true);
            mockUserService.Setup(u => u.Exists(secondUser.Id)).Returns(true);

            var mockUserFriendService = new Mock<IUserFriendService>();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.FriendRequests.Add(friendRequest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.SendFriendRequest(firstUser.Id, secondUser.Id));
            }
        }

        [TestMethod]
        public void Throw_When_UserDoesNotExist()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Throw_When_UserDoesNotExist));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            var mockUserService = new Mock<IUserService>();
            mockUserService.Setup(u => u.Exists(firstUser.Id)).Returns(true);

            var mockUserFriendService = new Mock<IUserFriendService>();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new FriendRequestService(assertContext, mockUserService.Object, mockUserFriendService.Object, mapper);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.SendFriendRequest(firstUser.Id, secondUser.Id));
            }
        }

    }
}
