﻿namespace SocialNetwork.Tests.CommentsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.CommentService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using System;

    [TestClass]
    public class CreateComment_Should
    {
        [TestMethod]
        public void Succsesfully_Add_Comment_For_Post()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Succsesfully_Add_Comment_For_Post));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("50ee8e13-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };

            var post = new Post
            {
                Id = 99,
                Content = "Test for a post"
            };

            var commentDTO = new CommentDTO
            {
                Id = 99,
                Content = "Test for a comment",
                PostId = 99,
                UserId = Guid.Parse("50ee8e13-16ce-4168-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.Posts.Add(post);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);
                var result = sut.AddComment(commentDTO);

                var actual = result.Result;

                var actualContent = actual.Content;
                var actualUser = actual.UserId;
                var actualPost = actual.PostId;

                Assert.AreEqual(commentDTO.Content, actualContent);
                Assert.AreEqual(commentDTO.PostId, actualPost);
                Assert.AreEqual(commentDTO.UserId, actualUser);
            }
        }

        [TestMethod]
        public void Throw_Exception_When_UserIsNotFound()
        {

            var options = SocialNetworkUtility.GetOptions(nameof(Throw_Exception_When_UserIsNotFound));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var post = new Post
            {
                Id = 199,
                Content = "Test for a post"
            };

            var commentDTO = new CommentDTO
            {
                Id = 299,
                Content = "Test for a comment",
                PostId = 199,
                UserId = Guid.Parse("50ee8e13-16ce-4aaa-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Posts.Add(post);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);
                var result = sut.AddComment(commentDTO);

                var actual = result.Exception.Message;
                var expected = "One or more errors occurred. (Value cannot be null. (Parameter 'User cannot be null'))";

                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod]
        public void Throw_Exception_When_PostIsNotFound()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Throw_Exception_When_PostIsNotFound));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("50ee8e13-16ce-4168-8c30-13e24e22297e"),
                UserName = "Georgi",
                Email = "georgssi@gmail.com"
            };

            var commentDTO = new CommentDTO
            {
                Id = 299,
                Content = "Test for a comment",
                PostId = 29,
                UserId = Guid.Parse("50ee8e13-16ce-4168-8c30-13e24e22297e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new CommentService(assertContext, mapper, date.Object);
                var result = sut.AddComment(commentDTO);

                var actual = result.Exception.Message;
                var expected = "One or more errors occurred. (Value cannot be null. (Parameter 'Post cannot be null'))";

                Assert.AreEqual(expected, actual);
            }
        }
    }
}

