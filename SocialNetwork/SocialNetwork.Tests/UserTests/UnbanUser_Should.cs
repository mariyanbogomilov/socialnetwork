﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Tests.UserTests
{
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.UserService;

    [TestClass]
    public class UnbanUser_Should
    {
        [TestMethod]
        public void UnbanUser_IsDeleted_ReturnFalse()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(UnbanUser_IsDeleted_ReturnFalse));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var user = SocialNetworkUtility.GetUsers().First();
            user.IsDeleted = true;

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.UnbanUser(user.Id);

                Assert.AreEqual(true, user.IsDeleted);
                Assert.AreEqual(false, assertContext.Users.First().IsDeleted);
            }
        }

        [TestMethod]
        public void UnbanUser_UserDoesNotExist_ThrowNullException()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(UnbanUser_UserDoesNotExist_ThrowNullException));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var user = SocialNetworkUtility.GetUsers().First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UnbanUser(user.Id));
            }
        }
    }
}
