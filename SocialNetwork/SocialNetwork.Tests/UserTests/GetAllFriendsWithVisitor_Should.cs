﻿namespace SocialNetwork.Tests.UserTests
{
    using System;
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.EntityDTOs;
    using Services.UserService;

    [TestClass]
    public class GetAllFriendsWithVisitor_Should
    {
        [TestMethod]
        public void GetAllFriends_WithVisitorThatIsNotHisFriend_ReturnFalse()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetAllFriends_WithVisitorThatIsNotHisFriend_ReturnFalse));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(3).First();
            var userFriend = SocialNetworkUtility.GetUserFriends().First();
            var secondUserFriend = SocialNetworkUtility.GetUserFriends().Skip(1).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Users.Add(thirdUser);
                arrangeContext.UserFriends.Add(userFriend);
                arrangeContext.UserFriends.Add(secondUserFriend);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetAllFriendsWithVisitor(firstUser.Id, thirdUser.Id, 5).Result;

                var visitor = act.First();

                Assert.AreEqual(false, visitor.AreFriends);
            }
        }

        [TestMethod]
        public void GetAllFriends_WithVisitorThatIsNotHisFriend_ReturnTrue()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetAllFriends_WithVisitorThatIsNotHisFriend_ReturnTrue));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(2).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(3).First();
            var userFriend = SocialNetworkUtility.GetUserFriends().Skip(1).First();
            var secondUserFriend = SocialNetworkUtility.GetUserFriends().Skip(2).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Users.Add(thirdUser);
                arrangeContext.UserFriends.Add(userFriend);
                arrangeContext.UserFriends.Add(secondUserFriend);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetAllFriendsWithVisitor(firstUser.Id, thirdUser.Id, 5).Result;

                var visitor = act.First();

                Assert.AreEqual(true, visitor.AreFriends);
            }
        }

        [TestMethod]
        public void GetAllFriends_WithVisitorUserDoesNotExist_ThrowNullException()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetAllFriends_WithVisitorUserDoesNotExist_ThrowNullException));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(2).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(secondUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() =>
                    sut.GetAllFriendsWithVisitor(firstUser.Id, secondUser.Id, 5));
            }
        }
    }
}
