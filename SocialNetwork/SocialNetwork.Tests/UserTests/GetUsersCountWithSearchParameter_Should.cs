﻿namespace SocialNetwork.Tests.UserTests
{
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.EntityDTOs;
    using Services.UserService;

    [TestClass]
    public class GetUsersCountWithSearchParameter_Should
    {
        [TestMethod]
        public void GetUsers_CorrectCount()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(GetUsers_CorrectCount));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(3).First();
            var thirdUser = SocialNetworkUtility.GetUsers().Skip(4).First();

            var searchParameter = "4";

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Users.Add(thirdUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetUsersCountWithSearchParameter(searchParameter);

                Assert.AreEqual(2, act);
            }
        }
    }
}
