﻿namespace SocialNetwork.Tests.UserTests
{
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.EntityDTOs;
    using Services.UserService;

    [TestClass]
    public class CheckIfRequestIsSent_Should
    {
        [TestMethod]
        public void ReturnTrue_When_RequestExists()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnTrue_When_RequestExists));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var friendRequest = SocialNetworkUtility.GetPendingFriendRequest().First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.FriendRequests.Add(friendRequest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.CheckIfRequestIsSent(firstUser.Id, secondUser.Id).Result;

                Assert.AreEqual(true, act);
            }
        }

        [TestMethod]
        public void ReturnFalse_When_RequestDoesNotExists()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnFalse_When_RequestDoesNotExists));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.CheckIfRequestIsSent(firstUser.Id, secondUser.Id).Result;

                Assert.AreEqual(false, act);
            }
        }
    }
}
