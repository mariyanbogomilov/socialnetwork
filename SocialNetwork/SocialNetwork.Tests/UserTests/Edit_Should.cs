﻿namespace SocialNetwork.Tests.UserTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data;
    using Data.Models;
    using Data.Models.Enums;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.EntityDTOs;
    using Services.UserService;

    [TestClass]
    public class Edit_Should
    {
        [TestMethod]
        public void EditUser_SetCorrectlyNewParameters()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(EditUserDoesNotExist_ThrowNullException));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();

            var firstUserDTO = new UserDTO
            {
                Id = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                FirstName = "TestUserFnEdited",
                LastName = "TestUserLnEdited",
                Email = "TestUserEmailEdited@yahoo.com",
                DateOfBirth = new DateTime(1996, 5, 20),
                Bio = "Test Bio Edited",
                JobTitle = "Test Job Edited",
                IsPrivate = true,
                PhotoName = "pictureEdited.png",
                Country = "France"
            };

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.Edit(firstUser.Id, firstUserDTO);

                var editedUser = assertContext.Users.First();

                Assert.AreEqual(firstUserDTO.FirstName, editedUser.FirstName);
                Assert.AreEqual(firstUserDTO.LastName, editedUser.LastName);
                Assert.AreEqual(firstUserDTO.Email, editedUser.Email);
                Assert.AreEqual(firstUserDTO.DateOfBirth, editedUser.DateOfBirth);
                Assert.AreEqual(firstUserDTO.Bio, editedUser.Bio);
                Assert.AreEqual(firstUserDTO.JobTitle, editedUser.JobTitle);
                Assert.AreEqual(firstUserDTO.IsPrivate, editedUser.IsPrivate);
                Assert.AreEqual(firstUserDTO.PhotoName, editedUser.PhotoName);
                Assert.AreEqual(firstUserDTO.Country, editedUser.Country);
            }
        }

        [TestMethod]
        public void EditUserDoesNotExist_ThrowNullException()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(EditUserDoesNotExist_ThrowNullException));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();

            var firstUserDTO = new UserDTO
            {
                Id = Guid.Parse("d2680b55-bb0d-4e0e-a069-987578b4473e"),
                FirstName = "TestUser1Fn",
                LastName = "TestUser1Ln",
                Email = "TestUser1@yahoo.com",
                DateOfBirth = new DateTime(1995, 5, 20),
                Gender = "Male",
                Bio = "Test1 Biography",
                JobTitle = "Test1 Job Title",
                IsPrivate = false,
                PhotoName = "profilePicture.png",
                Country = "France",
                Posts = new List<PostDTO>(),
                FriendRequestReceived = new List<FriendRequestReceivedDTO>()
            };

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Edit(firstUser.Id, firstUserDTO));
            }
        }
    }
}
