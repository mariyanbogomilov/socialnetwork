﻿namespace SocialNetwork.Tests.UserTests
{
    using System.Linq;
    using Data;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services.EntityDTOs;
    using Services.UserService;

    [TestClass]
    public class GetAllUserPosts_Should
    {
        [TestMethod]
        public void ReturnCorrectCountOfPublicPosts_Without_Visitor()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnCorrectCountOfPublicPosts_Without_Visitor));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var firstUserPost = SocialNetworkUtility.GetPosts().First();
            var secondUserPost = SocialNetworkUtility.GetPosts().Skip(4).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Posts.Add(firstUserPost);
                arrangeContext.Posts.Add(secondUserPost);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetAllUserPosts(firstUser.Id, null, 5).Result;

                Assert.AreEqual(2, act.Count());
                Assert.IsInstanceOfType(act.First(), typeof(PostDTO));
            }
        }

        [TestMethod]
        public void ReturnCorrectCountOfPublicPosts_With_Visitor_ThatIsNotHisFriend()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnCorrectCountOfPublicPosts_With_Visitor_ThatIsNotHisFriend));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var firstUserPublicPost = SocialNetworkUtility.GetPosts().First();
            var secondUserPublicPost = SocialNetworkUtility.GetPosts().Skip(4).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.Posts.Add(firstUserPublicPost);
                arrangeContext.Posts.Add(secondUserPublicPost);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetAllUserPosts(firstUser.Id, secondUser.Id, 5).Result;

                Assert.AreEqual(2, act.Count());
                Assert.IsInstanceOfType(act.First(), typeof(PostDTO));
            }
        }

        [TestMethod]
        public void ReturnCorrectCountOfAllPosts_With_Visitor_ThatIsHisFriend()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ReturnCorrectCountOfAllPosts_With_Visitor_ThatIsHisFriend));
            var mapperConfig = SocialNetworkUtility.MapperConfiguration();
            var mapper = mapperConfig.CreateMapper();

            var mockUserManager = SocialNetworkUtility.GetMockUserManager();

            var firstUser = SocialNetworkUtility.GetUsers().First();
            var secondUser = SocialNetworkUtility.GetUsers().Skip(1).First();
            var userFriend = SocialNetworkUtility.GetUserFriends().First();
            var firstUserPublicPost = SocialNetworkUtility.GetPosts().First();
            var secondUserPublicPost = SocialNetworkUtility.GetPosts().Skip(2).First();
            var thirdUserPrivatePost = SocialNetworkUtility.GetPosts().Skip(4).First();

            using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(firstUser);
                arrangeContext.Users.Add(secondUser);
                arrangeContext.UserFriends.Add(userFriend);
                arrangeContext.Posts.Add(firstUserPublicPost);
                arrangeContext.Posts.Add(secondUserPublicPost);
                arrangeContext.Posts.Add(thirdUserPrivatePost);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new UserService(assertContext, mapper, mockUserManager.Object);

                var act = sut.GetAllUserPosts(firstUser.Id, secondUser.Id, 5).Result;

                Assert.AreEqual(3, act.Count());
                Assert.IsInstanceOfType(act.First(), typeof(PostDTO));
            }
        }
    }
}
