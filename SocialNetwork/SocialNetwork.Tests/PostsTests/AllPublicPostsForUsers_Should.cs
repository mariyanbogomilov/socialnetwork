﻿namespace SocialNetwork.Tests.PostsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PostService;
    using SocialNetwork.Services.UserService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using SocialNetwork.Data.Models.Enums;
    using System.Linq;

    [TestClass]
    public class AllPublicPostsForUsers_Should
    {
        [TestMethod]
        public void Return_CorrectCount_For_AllPublicPostsForUser()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Return_CorrectCount_For_AllPublicPostsForUser));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var userOne = new User
            {
                Id = Guid.Parse("111e8e13-16ce-4168-8c30-12e244c4497e"),
                UserName = "Georgi",
                Email = "gosho@gmail.com"
            };

            var postOne = new PostDTO
            {
                Id = 49,
                Content = "Test for a post",
                UserId = Guid.Parse("111e8e13-16ce-4168-8c30-12e244c4497e"),
                AccessStatus = AccessStatus.Public
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(userOne);
                arrContext.SaveChanges();
            }
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);

                var createOne = sut.AddPost(postOne);

                var publicPosts = sut.GetAllPublicPosts(userOne.Id);
                var publicPostsCount = sut.GetAllPublicPostsCount(userOne.Id);

                var actualPublicPosts = publicPosts.Result;

                Assert.AreEqual(1, actualPublicPosts.Count());
                Assert.AreEqual(1, publicPostsCount);
            }
        }
    }
}
