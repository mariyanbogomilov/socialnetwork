﻿namespace SocialNetwork.Tests.PostsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.PostService;
    using SocialNetwork.Services.UserService;
    using System;

    [TestClass]
    public class AddPost_Should
    {
        [TestMethod]
        public void Succsesfully_Add_Post()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Succsesfully_Add_Post));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var userService = new Mock<IUserService>();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("50ee1113-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };

            var post = new PostDTO
            {
                Id = 99,
                Content = "Test for a post",
                UserId = Guid.Parse("50ee1113-16ce-4168-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);
                var result = sut.AddPost(post);

                var actual = result.Result;

                var actualContent = actual.Content;
                var actualUser = actual.UserId;

                Assert.AreEqual(post.Content, actualContent);
                Assert.AreEqual(post.UserId, actualUser);
            }
        }
        [TestMethod]
        public void Succsesfully_Add_Post_WithPicture()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Succsesfully_Add_Post_WithPicture));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var userService = new Mock<IUserService>();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("50ee1113-12ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "gorgi@gmail.com"
            };
            var photo = new Photo
            {
                Id = 1,
                PhotoName = "testPicture.png"
            };

            var post = new PostDTO
            {
                Id = 19,
                Content = "Test for a post",
                UserId = Guid.Parse("50ee1113-12ce-4168-8c30-13e244c4497e"),
                PhotoName = "testPicture.png"
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.Photos.Add(photo);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);
                var result = sut.AddPost(post);

                var actual = result.Result;

                var actualContent = actual.Content;
                var actualUser = actual.UserId;
                var actualPhoto = actual.PhotoName;

                Assert.AreEqual(post.Content, actualContent);
                Assert.AreEqual(post.UserId, actualUser);
                Assert.AreEqual(post.PhotoName, actualPhoto);
            }
        }
        [TestMethod]
        public void Succsesfully_Add_Post_WithVideo()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Succsesfully_Add_Post_WithVideo));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var userService = new Mock<IUserService>();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("10ee1113-12ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "gorgi@gmail.com"
            };

            var post = new PostDTO
            {
                Id = 29,
                Content = "Test for a post",
                UserId = Guid.Parse("10ee1113-12ce-4168-8c30-13e244c4497e"),
                VideoUrl = "https://www.youtube.com/watch?v=DHdkRvEzW84&list=RDxwsYvBYZcx4&index=3"
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);
                var result = sut.AddPost(post);

                var actual = result.Result;

                var actualContent = actual.Content;
                var actualUser = actual.UserId;
                var actualVideo = actual.VideoUrl;

                var expectedUrl = "https://www.youtube.com/embed/DHdkRvEzW84";
                Assert.AreEqual(post.Content, actualContent);
                Assert.AreEqual(post.UserId, actualUser);
                Assert.AreEqual(expectedUrl, actualVideo);
            }
        }
    }
}
