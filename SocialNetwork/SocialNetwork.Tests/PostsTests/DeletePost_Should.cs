﻿namespace SocialNetwork.Tests.PostsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PostService;
    using SocialNetwork.Services.UserService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using System;

    [TestClass]
    public class DeletePost_Should
    {
        [TestMethod]
        public void Successfully_Delete_Comment()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Successfully_Delete_Comment));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var user = new User
            {
                Id = Guid.Parse("1eee8e13-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "geori@gmail.com"
            };

            var post = new PostDTO
            {
                Id = 11,
                Content = "Test for a post",
                UserId = Guid.Parse("1eee8e13-16ce-4168-8c30-13e244c4497e"),
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object,userService.Object);

                var create = sut.AddPost(post);
                var delete = sut.DeletePost(11);
                var actual = delete.Result;

                Assert.AreEqual(true, actual);
            }
        }

        [TestMethod]
        public void Return_False_When_Delete_PostFails()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Return_False_When_Delete_PostFails));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object,userService.Object);
                var result = sut.DeletePost(111);

                var actual = result.Result;

                Assert.AreEqual(false, actual);
            }
        }
    }
}
