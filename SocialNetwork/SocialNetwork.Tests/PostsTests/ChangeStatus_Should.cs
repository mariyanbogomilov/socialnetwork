﻿namespace SocialNetwork.Tests.PostsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PostService;
    using SocialNetwork.Services.UserService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using SocialNetwork.Data.Models.Enums;

    [TestClass]
    public class ChangeStatus_Should
    {
        [TestMethod]
        public void ChangeStatus_To_Private()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ChangeStatus_To_Private));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var userOne = new User
            {
                Id = Guid.Parse("43ee8e13-16ce-4168-8c30-12e244c4497e"),
                UserName = "Georgi",
                Email = "gosho@gmail.com"
            };

            var postOne = new PostDTO
            {
                Id = 22,
                Content = "Test for a post",
                UserId = Guid.Parse("43ee8e13-16ce-4168-8c30-12e244c4497e"),
                AccessStatus = AccessStatus.Public
            };
            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(userOne);
                arrContext.SaveChanges();
            }
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);

                var createOne = sut.AddPost(postOne);

                var publicPosts = sut.ChangeStatus(22);
                var getPost = sut.GetPost(22);
                var actual = getPost.Result;

                Assert.AreEqual(AccessStatus.Private, actual.AccessStatus);
            }
        }
        [TestMethod]
        public void ChangeStatus_To_Public()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(ChangeStatus_To_Public));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var userOne = new User
            {
                Id = Guid.Parse("444e8e13-16ce-4168-8c30-12e244c4497e"),
                UserName = "Georgi",
                Email = "gosho@gmail.com"
            };

            var postOne = new PostDTO
            {
                Id = 23,
                Content = "Test for a post",
                UserId = Guid.Parse("444e8e13-16ce-4168-8c30-12e244c4497e"),
                AccessStatus = AccessStatus.Private
            };
            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(userOne);
                arrContext.SaveChanges();
            }
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);

                var createOne = sut.AddPost(postOne);

                var publicPosts = sut.ChangeStatus(23);
                var getPost = sut.GetPost(23);
                var actual = getPost.Result;

                Assert.AreEqual(AccessStatus.Public, actual.AccessStatus);
            }
        }
    }
}
