﻿namespace SocialNetwork.Tests.PostsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PostService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using SocialNetwork.Services.UserService;
    using SocialNetwork.Data.Models.Enums;
    using System.Linq;

    [TestClass]
    public class AllPublicPosts_Should
    {
        [TestMethod]
        public void Return_CorrectCount_For_AllPublicPosts()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Return_CorrectCount_For_AllPublicPosts));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var userOne = new User
            {
                Id = Guid.Parse("13ee8e13-16ce-4168-8c30-12e244c4497e"),
                UserName = "Georgi",
                Email = "gosho@gmail.com"
            };
            var userTwo = new User
            {
                Id = Guid.Parse("13e38e13-16ce-4168-8c30-12e244c4497e"),
                UserName = "Georgi",
                Email = "tosho@gmail.com"
            };

            var postOne = new PostDTO
            {
                Id = 49,
                Content = "Test for a post",
                UserId = Guid.Parse("13ee8e13-16ce-4168-8c30-12e244c4497e"),
                AccessStatus = AccessStatus.Public
            };

            var postTwo = new PostDTO
            {
                Id = 59,
                Content = "Test for a post",
                UserId = Guid.Parse("13e38e13-16ce-4168-8c30-12e244c4497e"),
                AccessStatus = AccessStatus.Public
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(userOne);
                arrContext.Users.Add(userTwo);
                arrContext.SaveChanges();
            }
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object,userService.Object);

                var createOne = sut.AddPost(postOne);
                var createTwo = sut.AddPost(postTwo);

                var publicPosts = sut.AllPublicPosts();
                var publicPostsCount = sut.AllPublicPostsCount();

                var actualPublicPosts = publicPosts.Result;

                Assert.AreEqual(2, actualPublicPosts.Count());
                Assert.AreEqual(2, publicPostsCount);
            }
        }
    }
}
