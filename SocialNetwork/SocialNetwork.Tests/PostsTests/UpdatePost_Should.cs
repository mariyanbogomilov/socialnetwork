﻿namespace SocialNetwork.Tests.PostsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PostService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.UserService;
    using System;

    [TestClass]
    public class UpdatePost_Should
    {
        [TestMethod]
        public void Succsesfully_Update_Post()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Succsesfully_Update_Post));
            var date = new Mock<IDateTimeProvider>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();
            var userService = new Mock<IUserService>();

            var user = new User
            {
                Id = Guid.Parse("50ee1123-16ce-4168-8c30-13e244c4497e"),
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };

            var post = new PostDTO
            {
                Id = 66,
                Content = "Test for a post",
                UserId = Guid.Parse("50ee1123-16ce-4168-8c30-13e244c4497e")
            };
            var newPost = new PostDTO
            {
                Id = 66,
                Content = "Test for updated post",
                UserId = Guid.Parse("50ee1123-16ce-4168-8c30-13e244c4497e")
            };

            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);
                var create = sut.AddPost(post);

                var update = sut.UpdatePost(66, newPost);

                var actualContent = update.Result.Content;

                Assert.AreEqual(newPost.Content, actualContent);
            }
        }


        [TestMethod]
        public void Return_CorrectMessage_When_Update_PostFails()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Return_CorrectMessage_When_Update_PostFails));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var post = new PostDTO
            {
                Id = 3,
                Content = "Faile to update"
            };
            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);
                var result = sut.UpdatePost(3,post);

                var actual = result.Exception.Message;
                var expected = "One or more errors occurred. (Value cannot be null.)";
                Assert.AreEqual(expected, actual);
            }
        }
    }
}
