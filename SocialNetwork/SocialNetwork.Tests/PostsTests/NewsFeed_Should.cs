﻿namespace SocialNetwork.Tests.PostsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.PostService;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.UserService;
    using System;
    using System.Linq;

    [TestClass]
    public class NewsFeed_Should
    {
        [TestMethod]
        public void Return_CorrectCount_For_NewsFeed()
        {
            var options = SocialNetworkUtility.GetOptions(nameof(Return_CorrectCount_For_NewsFeed));
            var date = new Mock<IDateTimeProvider>();
            var userService = new Mock<IUserService>();
            var config = SocialNetworkUtility.MapperConfiguration();
            var mapper = config.CreateMapper();

            var userOne = new User
            {
                Id = Guid.Parse("73ee8e13-16ce-4168-8c30-12e244c4497e"),
                UserName = "Georgi",
                Email = "gosho@gmail.com"
            };
            using (var arrContext = new SocialNetworkDbContext(options))
            {
                arrContext.Users.Add(userOne);
                arrContext.SaveChanges();
            }

            using (var assertContext = new SocialNetworkDbContext(options))
            {
                var sut = new PostService(assertContext, mapper, date.Object, userService.Object);

                var newsFeed = sut.Newsfeed(userOne.Id);
                var newsFeedCount = sut.NewsFeedCount(userOne.Id);

                var actualNewsFeed = newsFeed.Result;
                var count = newsFeedCount.Result;
                Assert.AreEqual(0, actualNewsFeed.Count());
                Assert.AreEqual(0, count);
            }
        }
    }
}
