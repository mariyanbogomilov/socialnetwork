﻿namespace SocialNetwork.Services.CommentService.Contract
{
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ICommentService
    {
        Task<IEnumerable<CommentDTO>> GetAllCommentsForPost(int id);
        Task<IEnumerable<CommentDTO>> GetFirstThreeCommentsForPost(int id);
        Task<CommentDTO> AddComment(CommentDTO commentDTO);
        Task<CommentDTO> UpdateComment(int id, CommentDTO commentDTO);
        Task<bool> DeleteComment(int id);
    }
}
