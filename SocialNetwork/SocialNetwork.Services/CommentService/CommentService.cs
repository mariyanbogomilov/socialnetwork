﻿namespace SocialNetwork.Services.CommentService
{
    using AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.CommentService.Contract;
    using SocialNetwork.Services.DTOsProvider.Contract;
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class CommentService : ICommentService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly SocialNetworkDbContext dbContext;
        private readonly IMapper _mapper;

        public CommentService(SocialNetworkDbContext dbContext, IMapper mapper
            , IDateTimeProvider dateTimeProvider)
        {
            this.dbContext = dbContext;
            this._mapper = mapper;
            this.dateTimeProvider = dateTimeProvider;
        }

        public async Task<IEnumerable<CommentDTO>> GetAllCommentsForPost(int id)
        {
            var comments = await this.dbContext.Comments
                .Where(c => !c.IsDeleted)
                .Include(post => post.Post)
                .Include(user => user.User)
                .Where(c => c.PostId == id && !c.IsDeleted)
                .OrderByDescending(c => c.CreatedOn)
                .Select(c => this._mapper.Map<CommentDTO>(c)).ToListAsync();

            return comments.Skip(3);
        }

        public async Task<CommentDTO> AddComment(CommentDTO commentDTO)
        {
            var user = await this.dbContext.Users
                .Where(u => !u.IsDeleted)
                .FirstOrDefaultAsync(u => u.Id == commentDTO.UserId);

            var post = await this.dbContext.Posts
                .Where(post => !post.IsDeleted)
                .FirstOrDefaultAsync(post => post.Id == commentDTO.PostId);
            if (post == null)
            {
                throw new ArgumentNullException("Post cannot be null");
            }
            if (user == null)
            {
                throw new ArgumentNullException("User cannot be null");
            }
            commentDTO.PostId = post.Id;
            commentDTO.UserId = user.Id;
            commentDTO.CreatedOn = dateTimeProvider.GetDateTime();
            commentDTO.IsDeleted = false;

            await this.dbContext.Comments.AddAsync(_mapper.Map<Comment>(commentDTO));
            await this.dbContext.SaveChangesAsync();
            return commentDTO;
        }

        public async Task<CommentDTO> UpdateComment(int id, CommentDTO commentDTO)
        {
            var comment = await this.dbContext.Comments
                .Where(p => !p.IsDeleted)
                .FirstOrDefaultAsync(comment => comment.Id == id);

            if (comment == null)
            {
                throw new ArgumentNullException();
            }
            else if (commentDTO.Content.Length < 6 || commentDTO.Content.Length > 300)
            {
                throw new ArgumentException("Content not in range");
            }

            comment.Content = commentDTO.Content;
            await this.dbContext.SaveChangesAsync();

            return commentDTO;
        }

        public async Task<bool> DeleteComment(int id)
        {
            try
            {
                var comment = await this.dbContext.Comments
                .Where(p => !p.IsDeleted)
                .FirstOrDefaultAsync(comment => comment.Id == id);

                comment.IsDeleted = true;
                comment.DeletedOn = dateTimeProvider.GetDateTime();

                await this.dbContext.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<IEnumerable<CommentDTO>> GetFirstThreeCommentsForPost(int id)
        {
            var comments = await this.dbContext.Comments
                .Where(c => !c.IsDeleted)
                .Include(post => post.Post)
                .Include(user => user.User)
                .Where(c => c.PostId == id && !c.IsDeleted)
                .OrderByDescending(c => c.CreatedOn).Take(3)
                .ToListAsync();

            var map = this._mapper.Map<IEnumerable<CommentDTO>>(comments);
            return map;
        }
    }
    //TODO: se dobavi delete za admin
}
