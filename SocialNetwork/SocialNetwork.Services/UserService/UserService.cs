﻿namespace SocialNetwork.Services.UserService
{
    using AutoMapper;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Internal;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Data.Models.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class UserService : IUserService
    {
        private readonly SocialNetworkDbContext context;
        private readonly IMapper mapper;
        private readonly UserManager<User> userManager;

        public UserService(SocialNetworkDbContext context, IMapper mapper,
            UserManager<User> userManager)
        {
            this.context = context;
            this.mapper = mapper;
            this.userManager = userManager;
        }

        public async Task<UserDTO> GetUser(Guid id)
        {
            var user = await this.context.Users
                .Include(u => u.ProfilePicture)
                .Include(u => u.Posts).ThenInclude(post => post.Photo)
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var userDto = this.mapper.Map<UserDTO>(user);

            userDto.FriendsCount = this.GetFriendsCount(id, Guid.Empty);

            return userDto;
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsers(string searchParameter = null, int? take = null, int skip = 0)
        {
            IQueryable<User> users;

            if (searchParameter == null)
            {
                users = this.context.Users
                    .OrderBy(u => u.FirstName)
                    .Skip(skip);
            }
            else
            {
                string parameterToLower = searchParameter.ToLower();

                users = this.context.Users
                    .Where(u => u.FirstName.Contains(searchParameter) || u.LastName.Contains(searchParameter) || u.Email.Contains(searchParameter))
                    .Skip(skip);
            }

            if (take.HasValue)
            {
                users = users.Take(take.Value);
            }

            var usersDto = await users.Select(u => this.mapper.Map<UserDTO>(u)).ToListAsync();

            return usersDto;
        }

        public int GetUsersCount()
        {
            return this.context.Users.Count();
        }

        public int GetUsersCountWithSearchParameter(string searchParameter)
        {
            return this.context.Users
                .Where(u => u.FirstName.Contains(searchParameter)
                || u.LastName.Contains(searchParameter)
                || u.Email.Contains(searchParameter))
                .Count();
        }

        public async Task<IEnumerable<PostDTO>> GetAllUserPosts(Guid id, Guid? visitorId, int? take = null, int skip = 0)
        {
            IQueryable<Post> posts;

            if (visitorId == Guid.Empty || visitorId == null)
            {
                posts = GetPublicPostsQuery(id);
            }
            else
            {
                if (await this.CheckIfFriends(id, visitorId.Value)
                    || id == visitorId.Value)
                {
                    posts = GetAllPostsQuery(id);
                }
                else
                {
                    posts = GetPublicPostsQuery(id);
                }
            }

            if (take.HasValue)
            {
                posts = posts.Skip(skip).Take(take.Value);
            }

            var postsDto = await posts.Select(p => this.mapper.Map<PostDTO>(p)).ToListAsync();

            return postsDto;
        }

        public async Task<int> GetPostsCount(Guid id, Guid? visitorId)
        {
            if (visitorId == Guid.Empty || visitorId == null)
            {
                return this.GetPublicPostsQuery(id).Count();
            }
            else
            {
                if (await this.CheckIfFriends(id, visitorId.Value)
                    || id == visitorId.Value)
                {
                    return this.GetAllPostsQuery(id).Count();
                }
                else
                {
                    return this.GetPublicPostsQuery(id).Count();
                }
            }
        }

        public async Task<IEnumerable<UserDTO>> GetAllFriendsWithVisitor(Guid userId, Guid visitorId, int? take = null, int skip = 0)
        {
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var friends = GetFriendsOfFirstRow(userId);

            var secondFriends = GetFriendsOfSecondRow(userId);

            var combinedFriends = Enumerable.Concat(friends, secondFriends)
                .Where(u => u.Id != visitorId)
                .OrderBy(n => n.FirstName)
                .Skip(skip);

            var friendsResult = combinedFriends.ToList();

            friendsResult.ForEach(f => f.AreFriends = f.AreFriends = this.CheckIfFriends(f.Id, visitorId).Result);

            return friendsResult.Take(take.Value);
        }

        public async Task<IEnumerable<UserDTO>> GetAllFriends(Guid userId, int? take = null, int skip = 0)
        {
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var friends = GetFriendsOfFirstRow(userId);

            var secondFriends = GetFriendsOfSecondRow(userId);

            var totalFriends = Enumerable.Concat(friends, secondFriends)
                .OrderBy(n => n.FirstName)
                .Skip(skip);

            if (take.HasValue)
            {
                totalFriends = totalFriends.Take(take.Value);
            }

            return totalFriends.ToList();
        }

        public async Task<UserDTO> Edit(Guid id, UserDTO userDTO)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == id && u.IsDeleted == false);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            if (userDTO.PhotoName != null)
            {
                user.PhotoName = userDTO.PhotoName;
            }

            if (userDTO.CurrentPassword != null && userDTO.NewPassword != null && userDTO.RepeatNewPassword != null)
            {
                var changeUserPassword = await this.userManager.ChangePasswordAsync(user, userDTO.CurrentPassword, userDTO.NewPassword);
            }

            user.FirstName = userDTO.FirstName;
            user.LastName = userDTO.LastName;
            user.Email = userDTO.Email;
            user.Country = userDTO.Country;
            user.DateOfBirth = userDTO.DateOfBirth;
            user.Bio = userDTO.Bio;
            user.JobTitle = userDTO.JobTitle;
            user.IsPrivate = userDTO.IsPrivate;

            await this.context.SaveChangesAsync();

            return userDTO;
        }

        public bool Exists(Guid id)
        {
            return this.context.Users.Any(u => u.Id == id && u.IsDeleted == false);
        }

        public async Task<bool> CheckIfFriends(Guid userId, Guid secondUserId)
        {
            return await this.context.UserFriends.AnyAsync(uf => (uf.UserId == userId && uf.FriendId == secondUserId)
                || (uf.FriendId == userId && uf.UserId == secondUserId));
        }

        public async Task<bool> CheckIfRequestIsSent(Guid userId, Guid secondUserId)
        {
            return await this.context.FriendRequests.AnyAsync(u => (u.SenderId == userId && u.ReceiverId == secondUserId)
                || (u.SenderId == secondUserId && u.ReceiverId == userId));
        }

        public int GetFriendsCount(Guid id, Guid? visitorId)
        {
            if (visitorId.HasValue)
            {
                return Enumerable.Concat(GetFriendsOfFirstRow(id), GetFriendsOfSecondRow(id))
                .Where(u => u.Id != visitorId)
                .Count();
            }
            else
            {
                return Enumerable.Concat(GetFriendsOfFirstRow(id), GetFriendsOfSecondRow(id))
                .Count();
            }
        }

        public async Task BanUser(Guid id)
        {
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            user.IsDeleted = true;

            await this.userManager.SetLockoutEnabledAsync(user, true);
            await this.userManager.SetLockoutEndDateAsync(user, DateTimeOffset.MaxValue);

            await this.context.SaveChangesAsync();
        }

        public async Task UnbanUser(Guid id)
        {
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            user.IsDeleted = false;

            await this.userManager.SetLockoutEnabledAsync(user, false);

            await this.context.SaveChangesAsync();
        }

        private IQueryable<UserDTO> GetFriendsOfFirstRow(Guid userId)
        {
            return this.context.UserFriends.Where(u => u.UserId == userId)
                .Select(u => this.mapper.Map<UserDTO>(u.Friend));
        }

        private IQueryable<UserDTO> GetFriendsOfSecondRow(Guid userId)
        {
            return this.context.UserFriends.Where(u => u.FriendId == userId)
                .Select(u => this.mapper.Map<UserDTO>(u.User));
        }
        private IQueryable<Post> GetPublicPostsQuery(Guid id)
        {
            return this.context.Posts
                .Include(post => post.Photo)
                .Include(post => post.User)
                .Include(post => post.Comments)
                .Include(post => post.Likes)
                .Where(p => p.IsDeleted == false && p.UserId == id && p.AccessStatus == AccessStatus.Public)
                .OrderByDescending(p => p.CreatedOn);
        }
        private IQueryable<Post> GetAllPostsQuery(Guid id)
        {
            return this.context.Posts
                .Include(post => post.Photo)
                .Include(post => post.User)
                .Include(post => post.Comments)
                .Include(post => post.Likes)
                .Where(p => p.IsDeleted == false && p.UserId == id)
                .OrderByDescending(p => p.CreatedOn);
        }
    }
}
