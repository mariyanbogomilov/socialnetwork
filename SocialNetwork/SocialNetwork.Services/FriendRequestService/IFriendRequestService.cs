﻿namespace SocialNetwork.Services.FriendRequestService
{
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IFriendRequestService
    {
        Task<IEnumerable<FriendRequestReceivedDTO>> UserReceivedRequests(Guid id, int? take = null, int skip = 0);
        Task<IEnumerable<FriendRequestSentDTO>> UserSentRequests(Guid id);
        Task SendFriendRequest(Guid senderId, Guid receiverId);
        Task<string> FriendRequestAction(Guid senderId, Guid receiverId, string requestStatus);
        Task CancelSentRequest(Guid senderId, Guid receiverId);
        bool Exists(Guid senderId, Guid receiverId);
        int GetReceivedRequestsCount(Guid id);
    }
}
