﻿namespace SocialNetwork.Services.EntityDTOs
{
    using System;
    using System.Collections.Generic;

    public class UserDTO
    {
        public Guid Id { get; set; }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Country { get; set; }
        public bool IsPrivate { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Bio { get; set; }
        public string JobTitle { get; set; }
        public string PhotoName { get; set; }
        public int? PhotoId { get; set; }
        public PhotoDTO ProfilePicture { get; set; }
        public ICollection<PostDTO> Posts { get; set; }
        public int FriendsCount { get; set; }
        public bool AreFriends { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string RepeatNewPassword { get; set; }
        public ICollection<FriendRequestReceivedDTO> FriendRequestReceived { get; set; }
    }
}
