﻿using SocialNetwork.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.EntityDTOs
{
    public class CommentDTO : IBaseDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int PostId { get; set; }
        public string Username { get; set; }
        public Guid UserId { get; set; }

        public virtual UserDTO User { get; set; }
        public DateTime CreatedOn { get ; set ; }
        public bool IsDeleted { get ; set ; }
        public DateTime? DeletedOn { get ; set ; }
    }
}
