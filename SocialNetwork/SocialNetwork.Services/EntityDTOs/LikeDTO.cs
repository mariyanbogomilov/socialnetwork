﻿using SocialNetwork.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.EntityDTOs
{
    public class LikeDTO
    {
        public int PostId { get; set; }

        public Guid UserId { get; set; }

        public LikeStatus LikeType { get; set; }
    }
}
