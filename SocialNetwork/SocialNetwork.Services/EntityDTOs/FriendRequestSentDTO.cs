﻿namespace SocialNetwork.Services.EntityDTOs
{
    using System;
    public class FriendRequestSentDTO
    {
        public Guid ReceiverId { get; set; }

        public string ReceiverName { get; set; }

        public string ReceiverPhotoName { get; set; }
    }
}
