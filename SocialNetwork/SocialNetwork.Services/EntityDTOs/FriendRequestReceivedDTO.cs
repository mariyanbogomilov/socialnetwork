﻿namespace SocialNetwork.Services.EntityDTOs
{
    using System;

    public class FriendRequestReceivedDTO
    {
        public Guid SenderId { get; set; }

        public string SenderName { get; set; }

        public string SenderPhotoName { get; set; }
    }
}
