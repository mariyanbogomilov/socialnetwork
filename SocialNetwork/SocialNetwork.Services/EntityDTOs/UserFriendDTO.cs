﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.EntityDTOs
{
    public class UserFriendDTO
    {
        public int Id { get; set; }

        public string FullName { get; set; }
    }
}
