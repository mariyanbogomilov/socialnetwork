﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.EntityDTOs
{
    public class PhotoDTO
    {
        public int Id { get; set; }
        public string PhotoName { get; set; }
        public IFormFile PhotoFile { get; set; }
    }
}
