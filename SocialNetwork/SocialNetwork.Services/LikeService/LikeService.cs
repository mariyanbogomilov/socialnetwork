﻿namespace SocialNetwork.Services.LikeService
{
    using AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Data.Models.Enums;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.LikeService.Contract;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class LikeService : ILikeService
    {
        private readonly SocialNetworkDbContext dbContext;

        public LikeService(SocialNetworkDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public int GetVotes(int postId)
        {
            var countLikes = 
                this.dbContext.Likes
                .Where(post => post.PostId == postId && post.LikeType == LikeStatus.UpLike);

            return countLikes.Count();
        }

        public async Task LikeAsync(int postId, Guid userId)
        {
            var like = await this.dbContext.Likes
                .FirstOrDefaultAsync(like => like.PostId == postId && like.UserId == userId);

            if (like != null)
            {
                var isUplike = like.LikeType == LikeStatus.UpLike;
                like.LikeType = isUplike ? LikeStatus.Neutral : LikeStatus.UpLike;
            }
            else
            {
                like = new Like
                {
                    PostId = postId,
                    UserId = userId,
                    LikeType = LikeStatus.UpLike
                };

                await this.dbContext.Likes.AddAsync(like);
            }
            await this.dbContext.SaveChangesAsync();
        }
    }
}
