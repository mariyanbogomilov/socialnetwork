﻿namespace SocialNetwork.Services.LikeService.Contract
{
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using System.Threading.Tasks;

    public interface ILikeService
    {
        Task LikeAsync(int postId, Guid userId);
        int GetVotes(int postId);
    }
}
