﻿namespace SocialNetwork.Services.UserFriend
{
    using AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using SocialNetwork.Data;
    using SocialNetwork.Data.Models;
    using SocialNetwork.Services.EntityDTOs;
    using SocialNetwork.Services.UserService;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class UserFriendService : IUserFriendService
    {
        private readonly SocialNetworkDbContext context;
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UserFriendService(SocialNetworkDbContext context, IUserService userService, IMapper mapper)
        {
            this.context = context;
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task AddFriend(Guid senderId, Guid receiverId)
        {
            var userFriend = new UserFriend
            {
                UserId = senderId,
                FriendId = receiverId
            };

            await this.context.UserFriends.AddAsync(userFriend);
            await this.context.SaveChangesAsync();
        }

        public async Task<bool> RemoveFriend(Guid localUserId, Guid friendToRemoveId)
        {
            bool localUserExists = this.userService.Exists(localUserId);

            bool friendExists = this.userService.Exists(friendToRemoveId);

            bool areFriends = await this.userService.CheckIfFriends(localUserId, friendToRemoveId);

            if (localUserExists == false || friendExists == false || areFriends == false)
            {
                return false;
            }

            UserFriend userFriend = ReturnCorrectUserFriendToRemove(localUserId, friendToRemoveId);

            FriendRequest friendRequest = ReturnCorrectFriendRequestToRemove(localUserId, friendToRemoveId);

            this.context.FriendRequests.Remove(friendRequest);
            this.context.UserFriends.Remove(userFriend);
            this.context.SaveChanges();

            return true;
        }

        private UserFriend ReturnCorrectUserFriendToRemove(Guid localUserId, Guid friendToRemoveId)
        {
            var userSide = this.context.UserFriends
                .FirstOrDefault(uf => uf.UserId == localUserId && uf.FriendId == friendToRemoveId);

            var friendSide = this.context.UserFriends
                .FirstOrDefault(uf => uf.UserId == friendToRemoveId && uf.FriendId == localUserId);

            return userSide ?? friendSide;
        }

        private FriendRequest ReturnCorrectFriendRequestToRemove(Guid localUserId, Guid friendToRemoveId)
        {
            var userSideRequest = this.context.FriendRequests
                .FirstOrDefault(fq => fq.SenderId == localUserId && fq.ReceiverId == friendToRemoveId);

            var friendSideRequest = this.context.FriendRequests
                .FirstOrDefault(fq => fq.SenderId == friendToRemoveId && fq.ReceiverId == localUserId);

            return userSideRequest ?? friendSideRequest;
        }
    }
}
