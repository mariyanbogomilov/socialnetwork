﻿namespace SocialNetwork.Data.Models.Enums
{
    public enum FriendRequestStatus
    {
        Pending = 0,
        Accepted = 1,
        Declined = 2,
    }
}
