﻿namespace SocialNetwork.Data.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abstracts;
    using SocialNetwork.Data.Models.Enums;

    public class Comment : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int PostId { get; set; }

        public virtual Post Post { get; set; }

        [Required]
        public string Content { get; set; }

        public Guid UserId { get; set; }

        public virtual User User { get; set; }
    }
}