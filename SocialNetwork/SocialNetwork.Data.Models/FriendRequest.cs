﻿namespace SocialNetwork.Data.Models
{
    using System;
    using Enums;

    public class FriendRequest
    {
        public Guid SenderId { get; set; }
        public User Sender { get; set; }

        public Guid ReceiverId { get; set; }
        public User Receiver { get; set; }

        public FriendRequestStatus FriendRequestStatus { get; set; }

    }
}
