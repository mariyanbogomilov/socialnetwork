﻿namespace SocialNetwork.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abstracts;
    using SocialNetwork.Data.Models.Enums;

    public class Post : BaseEntity
    {
        public Post()
        {
            this.Comments = new HashSet<Comment>();
            this.Likes = new HashSet<Like>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        public AccessStatus AccessStatus { get; set; }
        public int? PhotoId { get; set; }
        public Photo Photo { get; set; }
        public string VideoUrl { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Like> Likes { get; set; }
    }
}