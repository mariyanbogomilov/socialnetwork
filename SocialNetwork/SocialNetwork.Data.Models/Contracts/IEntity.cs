﻿namespace SocialNetwork.Data.Models.Contracts
{
    using System;

    public interface IEntity
    {
        DateTime CreatedOn { get; set; }

        bool IsDeleted { get; set; }

        DateTime? DeletedOn { get; set; }
    }
}
