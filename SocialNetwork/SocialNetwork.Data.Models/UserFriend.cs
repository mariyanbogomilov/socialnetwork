﻿namespace SocialNetwork.Data.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class UserFriend
    {
        [Key]
        public int Id { get; set; }

        public Guid UserId { get; set; }

        public virtual User User { get; set; }

        public Guid FriendId { get; set; }

        public virtual User Friend { get; set; }
    }
}