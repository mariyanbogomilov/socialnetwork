﻿namespace SocialNetwork.Data.Models
{
    using Microsoft.AspNetCore.Identity;
    using System;
    using System.Collections.Generic;
    using Contracts;
    using SocialNetwork.Data.Models.Enums;

    public class User : IdentityUser<Guid>, IEntity
    {
        public User()
        {
            this.Comments = new HashSet<Comment>();
            this.Likes = new HashSet<Like>();
            this.Friends = new HashSet<UserFriend>();
            this.Posts = new HashSet<Post>();
            this.FriendRequestReceived = new HashSet<FriendRequest>();
            this.FriendRequestSent = new HashSet<FriendRequest>();
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public Gender Gender { get; set; }
        public string Bio { get; set; }
        public string JobTitle { get; set; }
        public bool IsPrivate { get; set; }
        public int? PhotoId { get; set; }
        public string PhotoName { get; set; }
        public string Country { get; set; }

        public Photo ProfilePicture { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<UserFriend> Friends { get; set; }

        public virtual ICollection<FriendRequest> FriendRequestSent { get; set; }

        public virtual ICollection<FriendRequest> FriendRequestReceived { get; set; }

        public virtual ICollection<Like> Likes { get; set; }
    }
}