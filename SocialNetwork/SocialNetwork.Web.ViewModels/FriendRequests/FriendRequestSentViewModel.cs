﻿namespace SocialNetwork.Web.ViewModels.FriendRequests
{
    using System;

    public class FriendRequestSentViewModel
    {
        public Guid ReceiverId { get; set; }

        public string ReceiverName { get; set; }
    }
}
