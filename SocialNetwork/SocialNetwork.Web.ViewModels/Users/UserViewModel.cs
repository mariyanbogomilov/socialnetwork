﻿namespace SocialNetwork.Web.ViewModels.Users
{
    using SocialNetwork.Services.EntityDTOs;
    using System;
    using System.Collections.Generic;

    public class UserViewModel
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public PhotoDTO ProfilePicture { get; set; }
        public ICollection<PostDTO> Posts { get; set; }
        public string FullName => $"{this.FirstName} {this.LastName}";
    }
}
