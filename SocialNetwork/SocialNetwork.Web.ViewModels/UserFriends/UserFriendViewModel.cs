﻿namespace SocialNetwork.Web.ViewModels.UserFriends
{
    public class UserFriendViewModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }
    }
}
